﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueHandler : MonoBehaviour
{
    //load in dialogue text files and store within a struct that holds dialogue array, resource given (if given)

    [SerializeField]
    List<DialogueData> allDialogue = new List<DialogueData>();

    SceneDialogue newDialogue;

    
    //Pass in the ID of the dialogue wanted, check each entry to find the right one to load
    public void startDialogue(int d, SceneDialogue s)
    {
        newDialogue = s;

        foreach(DialogueData data in allDialogue)
        {
            if(data.ID == d)
            {
                newDialogue.assignValues(data.dialogue, data.resourcesObtained, data.peopleGiven);
                break;
            }
        }
    }

   
}
