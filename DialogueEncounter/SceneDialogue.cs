﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InputSamples.Gestures;
using TMPro;

public class SceneDialogue : EncounterSceneLogic
{
    bool showingText = false;

    [SerializeField]
    [Range(0.01f, 1)]
    float textSpeed = 0;

    static char[] specialCharacters = "<>".ToCharArray();

    [SerializeField]
    List<int> allSpecial = new List<int>();
    IEnumerator DisplayDialogueText(string d)
    {
        int ind = 0;
        string newT = "";

        showingText = true;

        for (int i = 0; i < d.Length; i++)
        {
            int index = d.IndexOfAny(specialCharacters, i);

            if (i == index)
            {
                allSpecial.Add(i);
            }
        }

        while (showingText)
        {
            if (ind > d.Length - 1)
            {
                showingText = false;
            }
            else
            {
                if (allSpecial.Count > 1)
                {
                    if (ind == allSpecial[0])
                    {
                        int jump = allSpecial[1] - allSpecial[0];
                        ind += (jump + 1);
                        allSpecial.RemoveAt(0);
                        allSpecial.RemoveAt(0);
                    }
                }

                if (ind <= d.Length)
                {
                    newT += d[ind];
                }
                

                dialogueText.text = newT;


                ind++;


                playDialogueSound();
            }
            yield return new WaitForSeconds(textSpeed);
        }
    }

    string[] dialogue;
    List<DialogueData.ResourcesGiven> resources = new List<DialogueData.ResourcesGiven>();

    [SerializeField]
    GameObject dialogueCanvas;

    int dialogueTracker = 0;
    [SerializeField]
    TextMeshProUGUI dialogueText;

    [SerializeField]
    Image sceneCharacterLeft;

    [SerializeField]
    Image sceneCharacterRight;

    [SerializeField]
    Image tutorialImage;

    int peopleGiven = 0;

    HubHandler hubHandler;
    DialogueHandler dialogueHandler;

    DayManager dayManager;

    List<DialogueData.DialogueText> dialogueInfo = new List<DialogueData.DialogueText>();

    //Access to all tutorial buttons within this scene
    TutorialButton[] allSceneButtons;
    TutorialButton activeButton;
    GameObject storedButton;

    [SerializeField]
    GameObject box;

    [SerializeField]
    Sprite dialogueBoxLeft;

    [SerializeField]
    Sprite dialogueBoxRight;

    [SerializeField]
    Sprite tutorialBox;

    [SerializeField]
    GameObject spriteBlackout;

    [SerializeField]
    GameObject uiBlackout;

    private GestureController gestureController;

    // Start is called before the first frame update
    void Start()
    {
        uiBlackout.SetActive(false);
        spriteBlackout.SetActive(false);

        gestureController = GameManager.Instance.gestureController;
        gestureController.Pressed += PressedScreen;
    }

    void PressedScreen(SwipeInput swipeInput)
    {
        next();
    }

    private void OnDisable()
    {
        gestureController.Pressed -= PressedScreen;
    }

    public override void StartEncounter(SpawnedEncounter encounterSpawned)
    {
        GameManager.Instance.blockRaycasts();

        base.StartEncounter(encounterSpawned); //This should always be the FIRST line of this method

        hubHandler = GameManager.Instance.hubHandler;
        dialogueHandler = GameManager.Instance.dialogueHandler;
        dayManager = GameManager.Instance.dayManager;

        allSceneButtons = Resources.FindObjectsOfTypeAll(typeof(TutorialButton)) as TutorialButton[];


        dialogueHandler.startDialogue(encounterSpawned.getData().dialogueID, this);
    }

    public override void FinishEncounter()
    {
        GameManager.Instance.hideBlocker();

        Debug.Log("Encounter finished");

        if (storedButton != null)
        {
            Destroy(activeButton);
            storedButton.SetActive(true);
            activeButton = null;
            storedButton = null;
        }

        base.FinishEncounter(); //This should always be the LAST line of this method.
    }

    public void assignValues(List<DialogueData.DialogueText> d, List<DialogueData.ResourcesGiven> r, int p)
    {
        if (d.Count > 0)
        {
            dialogue = new string[d.Count];

            for (int i = 0; i < d.Count; i++)
            {
                dialogue[i] = d[i].dialogue;
            }

            resources = r;
            peopleGiven = p;

            dialogueTracker = 0;

            dialogueInfo = d;

            showDialogue(true);
        }
        else
        {
            FinishEncounter();
        }
    }

    //Update the dialogue text on screen
    void showDialogue(bool writeOut)
    {       
        if (writeOut)
        {
            StartCoroutine(DisplayDialogueText(dialogue[dialogueTracker]));
        }
        else
        {
            StopAllCoroutines();
            showingText = false;
            dialogueText.text = dialogue[dialogueTracker];
        }

        if (!dialogueInfo[dialogueTracker].SameSprites)
        {
            if (dialogueInfo[dialogueTracker].characterLeft != null)
            {
                sceneCharacterLeft.sprite = dialogueInfo[dialogueTracker].characterLeft;
                sceneCharacterLeft.gameObject.SetActive(true);
            }
            else
            {
                sceneCharacterLeft.gameObject.SetActive(false);
            }

            if (dialogueInfo[dialogueTracker].characterRight != null)
            {
                sceneCharacterRight.sprite = dialogueInfo[dialogueTracker].characterRight;
                sceneCharacterRight.gameObject.SetActive(true);
            }
            else
            {
                sceneCharacterRight.gameObject.SetActive(false);
            }

            if (dialogueInfo[dialogueTracker].tutorialImage != null)
            {
                tutorialImage.sprite = dialogueInfo[dialogueTracker].tutorialImage;
                tutorialImage.gameObject.SetActive(true);
            }
            else
            {
                tutorialImage.gameObject.SetActive(false);
            }
        }

        if (dialogueInfo[dialogueTracker].RequiresTutorialSprite && allSceneButtons.Length > 0)
        {
            foreach(TutorialButton t in allSceneButtons)
            {
                if(t.getButtonName() == dialogueInfo[dialogueTracker].buttonName)
                {
                    if (storedButton != null)
                    {
                        Destroy(activeButton);
                        storedButton.SetActive(true);
                        activeButton = null;
                        storedButton = null;
                    }

                    if (t.GetComponent<Image>() != null)
                    {
                        activeButton = Instantiate(t);
                        activeButton.GetComponent<RectTransform>().position = t.GetComponent<RectTransform>().position;
                        activeButton.GetComponent<Image>().color = Color.white;;
                        storedButton = t.gameObject;
                        storedButton.SetActive(false);


                        uiBlackout.SetActive(true);
                        spriteBlackout.SetActive(false);

                        activeButton.transform.SetParent(dialogueCanvas.transform);
                    }
                    else
                    {
                        spriteBlackout.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0);

                        activeButton = Instantiate(t);
                        activeButton.transform.position = t.transform.position;
                        activeButton.GetComponent<SpriteRenderer>().sortingOrder = 5;
                        storedButton = t.gameObject;
                        storedButton.SetActive(false);


                        uiBlackout.SetActive(false);
                        spriteBlackout.SetActive(true);

                        activeButton.transform.SetParent(spriteBlackout.transform);
                    }

                    break;
                }
            }
        }
        else
        {
            uiBlackout.SetActive(false);
            spriteBlackout.SetActive(false);
        }

        if (dialogueInfo[dialogueTracker].isDialogueLine)
        {
            if (dialogueInfo[dialogueTracker].leftDialogue)
            {
                box.GetComponent<Image>().sprite = dialogueBoxLeft;
            }
            else
            {
                box.GetComponent<Image>().sprite = dialogueBoxRight;
            }
        }
        else
        {
            box.GetComponent<Image>().sprite = tutorialBox;
        }
    }

    //Move onto the next line of the dialogue
    public void next()
    {
        if (storedButton != null)
        {
            Destroy(activeButton);
            storedButton.SetActive(true);
            activeButton = null;
            storedButton = null;
        }

        if (dialogueTracker < dialogue.Length - 1)
        {
            if (!showingText)
            {
                dialogueTracker += 1;
                showDialogue(true);
            }
            else
            {
                showDialogue(false);
            }
        }
        else
        {         

            showDialogue(false);
            givePlayerResources();
            FinishEncounter();
        }
   
    }

    //After the dialogue has ended give the player the intended resources
    void givePlayerResources()
    {
        foreach(DialogueData.ResourcesGiven r in resources)
        {
            hubHandler.addResource(r.amount, r.type);
        }

        //Add new people to the hub
        if(peopleGiven > 0)
        {
            dayManager.FoundNewPeople(peopleGiven);
        }
    }

    void playDialogueSound()
    {

    }

}
