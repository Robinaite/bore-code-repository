﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "DialogueData", menuName = "DialogueEncounter/Dialogue", order = 0)]
public class DialogueData : SerializedScriptableObject
{
	

	[System.Serializable]
	public struct DialogueText
	{
		[ToggleGroup("SameSprites")]
		[HideIf("SetSprites")]
		public bool SameSprites;

		[ToggleGroup("SetSprites")]
		[HideIf("SameSprites")]
		public bool SetSprites;

		//DONT TOUCH THIS, IT WILL DELETE THE DIALOGUE DATA
		[SerializeField]
		public string dialogue;

		[ShowIf("SetSprites")]
		public Sprite characterLeft;
		[ShowIf("SetSprites")]
		public Sprite characterRight;
		[ShowIf("SetSprites")]
		public Sprite tutorialImage;

		GameObject icon;

		[ToggleGroup("RequiresTutorialSprite")]
		public bool RequiresTutorialSprite;

		[ShowIf("RequiresTutorialSprite")]
		public string buttonName;

		public bool isDialogueLine;

		[ShowIf("isDialogueLine")]
		public bool leftDialogue;

	}

	[SerializeField]
	public List<DialogueText> dialogue = new List<DialogueText>();

	[System.Serializable]
	public struct ResourcesGiven
	{
		[SerializeField]
		public ResourceType.Types type;
		[SerializeField]
		public int amount;
	}

	[SerializeField]
	public List<ResourcesGiven> resourcesObtained = new List<ResourcesGiven>();

	[SerializeField]
	public int peopleGiven = 0;

	[SerializeField]
	public int ID;
}
