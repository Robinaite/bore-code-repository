﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaftLink : MonoBehaviour
{
    GameManager manager;
    ShaftHandler handler;

    [SerializeField]
    WireframeCameraDrag cam;

    [SerializeField]
    GameObject shaftParent;

    public EncounterData spawnEncounterFirstClick;
    

    HubHandler hub;
    // Start is called before the first frame update
    void Start()
    {
        hub = GameManager.Instance.hubHandler;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        manager = GameManager.Instance;
        handler = GameManager.Instance.shaftHandler;

        handler.setCamera(cam);
        handler.setParent(shaftParent);
        manager.setShaftsActive(true);
    }

    private void OnDisable()
    {
        if (manager)
        {
            manager.setShaftsActive(false);
        }
    }

    public void spawn()
    {
        manager.showCavern();
        if (!GameManager.Instance.hasSpawnedShaftEncounter)
        {
            GameManager.Instance.hasSpawnedShaftEncounter = true;
            SpawnedEncounter encounter = new SpawnedEncounter(spawnEncounterFirstClick);
            encounter.StartEncounter(null,null);
        }
    }
}
