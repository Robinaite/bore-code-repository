﻿using System;
using UnityEngine;

namespace Shafts
{
    public class Shaft : MonoBehaviour
    {
        public GameObject cavern;

        public Animator shaftAnimator;
        
        private ResourceType.Types type;

        private int cost;

        private ResourceType.Types type2;
  
        private int cost2;

        private HubHandler hubHandler;

        private bool unlockedCavern = false;

        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(this);
            hubHandler = GameManager.Instance.hubHandler;
        }

        private void OnEnable()
        {
            shaftAnimator.SetBool("Unlocked",unlockedCavern);
        }

        public void SetupShaft(GameManager.Shaft shaftCosts)
        {
            type = shaftCosts.type;
            cost = shaftCosts.cost;
            type2 = shaftCosts.type2;
            cost2 = shaftCosts.cost2;
        }

        private void Update()
        {
            if (!unlockedCavern && hubHandler.checkAvaliableResource(cost, type) && hubHandler.checkAvaliableResource(cost2, type2))
            {
                shaftAnimator.SetBool("Unlockable",true);
            }
            else if(!unlockedCavern)
            {
                shaftAnimator.SetBool("Unlockable",false);
            }
        }

        public void ShowCavern()
        {
            unlockedCavern = true;
            cavern.SetActive(true);
            shaftAnimator.SetBool("Unlocked",unlockedCavern);
            GameManager.Instance.AddNewShaft();
        }

        public bool IsShowingCavern()
        {
            return cavern.activeInHierarchy;
        }
        
        

    }
}
