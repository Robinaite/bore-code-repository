﻿using System.Collections;
using System.Collections.Generic;
using Shafts;
using UnityEngine;

public class ShaftHandler : MonoBehaviour
{

    GameObject newShaft;

    Vector3 lastSpawnPos;

    GameObject shaftParent;

    WireframeCameraDrag cam;

    GameManager manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameManager.Instance;
    }
    
    public void StartGame()
    {
        lastSpawnPos = new Vector3(0, 4.4f, 0);
    }

    public void setParent(GameObject p)
    {
        shaftParent = p;
    }

    //Go to the next avaliable shaft spawn and integrate into the scene
    public void spawnNewShaft(GameObject newS, float size, GameObject p, GameManager.Shaft shaftCosts)
    {
        //Create the new shaft wih the avaliable caverns 
        newShaft = Instantiate(newS);

        //get the size of the shaft to spawn in the correct pos
        Vector3 spawn = lastSpawnPos - new Vector3(0, (newShaft.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y / 2) - 0.05f, 0);

        //Update the new spawn point after spawning in shaft
        lastSpawnPos = spawn - new Vector3(0, (newShaft.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y / 2) - 0.05f, 0);

        newShaft.transform.position = spawn;

        newShaft.transform.parent = p.transform;
        
        newShaft.GetComponent<Shaft>().SetupShaft(shaftCosts);
        
        cam.updateBottomThreshold(size);

        manager.updateShaft(newShaft);
    }

    public void updateCamera(float s)
    {
        cam.updateBottomThreshold(s);
    }

    public void setCamera(WireframeCameraDrag c)
    {
        cam = c;
    }

    
}
