﻿using System;
using System.Collections;
using System.Collections.Generic;
using InputSamples.Gestures;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonBehaviour<GameManager>
{    
    [BoxGroup("Managers")]
    [SerializeField]
    public GestureController gestureController;
    
    [BoxGroup("Managers")]
    [SerializeField]
    public SceneLoader sceneLoader;

    [BoxGroup("Managers")]
    [SerializeField] 
    public HubHandler hubHandler;

    [BoxGroup("Managers")]
    [SerializeField]
    public DayManager dayManager;

    [BoxGroup("Managers")]
    [SerializeField]
    public ShaftHandler shaftHandler;

    [BoxGroup("Managers")]
    [SerializeField]
    public GlobalCanvasUI globalCanvasUI;

    [BoxGroup("Managers")]
    [SerializeField]
    public DialogueHandler dialogueHandler;
    [BoxGroup("Managers")]
    [SerializeField]
    public AudioManager audioManager;
    [BoxGroup("Managers")]
    [SerializeField]
    public MouseScript mouseScript;

    private List<Cavern> gameCaverns = new List<Cavern>();

    [System.Serializable]
    public struct Shaft
    {
        [SerializeField]
        public GameObject prefab;
        [SerializeField]
        public float size;
        [SerializeField]
        public ResourceType.Types type;
        [SerializeField]
        public int cost;
        [SerializeField]
        public ResourceType.Types type2;
        [SerializeField]
        public int cost2;
    }

    [BoxGroup("Shafts")]
    [SerializeField]
    List<Shaft> spawnableShafts = new List<Shaft>();

    [SerializeField,HideInInspector]
    List<Shaft> spawned = new List<Shaft>();

    [SerializeField]
    [BoxGroup("Shafts")]
    GameObject shaftsParent;

    [SerializeField]
    [BoxGroup("Encounter")]
    private EncounterData startGameEncounter;

    bool spawnedShaftTutorial = false;

    public int shaftsSpawnedCount => spawned.Count;

    public int currentSpawned { get; private set; } = 0;

    [SerializeField]
    [BoxGroup("Audio")]
    private AK.Wwise.Event buildShaftEvent;
    [SerializeField]
    [BoxGroup("Audio")]
    private AK.Wwise.Event unableToBuildShaft;

    [SerializeField]
    GameObject blocker;

    public struct SpawnedCavern
    {
        public Cavern cavern;
        public bool spawned;
    }

    List<SpawnedCavern> spawnedCaverns = new List<SpawnedCavern>();
    
    public Vector3 cameraPositionSaved;
    public bool firstTimeSetup = false;
    public bool hasSpawnedShaftEncounter = false;
    private void Awake()
    {
        if (!gestureController)
        {
            gestureController = GetComponentInChildren<GestureController>();
        }

        if (!sceneLoader)
        {
            sceneLoader.GetComponent<SceneLoader>();
        }

        if (!hubHandler)
        {
            hubHandler = GetComponent<HubHandler>();
        }
        
        if (!dayManager)
        {
            dayManager = GetComponent<DayManager>();
        }

        if(!shaftHandler)
        {
            shaftHandler = GetComponent<ShaftHandler>();
        }
        if(!globalCanvasUI)
        {
            globalCanvasUI = GetComponent<GlobalCanvasUI>();
        }

        if(!dialogueHandler)
        {
            dialogueHandler = GetComponent<DialogueHandler>();
        }

        if(!mouseScript)
        {
            mouseScript = GetComponent<MouseScript>();
        }
        
        if(!audioManager)
        {
            audioManager = GetComponent<AudioManager>();
        }

        blocker.SetActive(false);
        
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "Hub")
        {
            SetupStartGame();
        }
    }

    public void StartGame()
    {
        if (startGameEncounter)
        {
            SpawnedEncounter encounter = new SpawnedEncounter(startGameEncounter);
            encounter.StartEncounter(null,InitializeGame,true);
            ToggleEncounterState(true);
        }
        else
        {
            sceneLoader.OnSceneLoadBeforeSceneChanger += SetupStartGame;
            sceneLoader.LoadScene("Hub");
        }

    }

    private void InitializeGame()
    {
        SetupStartGame();
    }

    private void SetupStartGame()
    {

        firstTimeSetup = false;
        GameManager.Instance.hasSpawnedShaftEncounter = false;
        setSpawnedTutorial(false);
        sceneLoader.OnSceneLoadBeforeSceneChanger -= SetupStartGame;
        
        for (int i = spawned.Count - 1; i >= 0; i--)
        {
            Shaft shaft = spawned[i];
            spawned.RemoveAt(i);
            Destroy(shaft.prefab);
        }

        currentSpawned = 0;
        hubHandler.StartGame();
        spawned.Clear();
        gameCaverns.Clear();
        shaftHandler.StartGame();
        dayManager.StartGame();
        
    }


    public Cavern CreateNewCavern(CavernData cavernData, CavernWireframe cavernWireframe)
    {
        Cavern cavern = new Cavern(cavernData,cavernWireframe);
        gameCaverns.Add(cavern);
        return cavern;
    }

    public Cavern ObtainCavernFromData(CavernWireframe cavernData)
    {
        return gameCaverns.Find(cavern => cavern.IsOfCavernWireframe(cavernData));
    }

    public void showCavern()
    {
        if (currentSpawned < spawnableShafts.Count)
        {
            if (hubHandler.checkAvaliableResource(spawnableShafts[currentSpawned].cost, spawnableShafts[currentSpawned].type) && hubHandler.checkAvaliableResource(spawnableShafts[currentSpawned].cost2, spawnableShafts[currentSpawned].type2))
            {
                if (buildShaftEvent.IsValid())
                {
                    buildShaftEvent.Post(gameObject);
                }
                hubHandler.takeResource(spawnableShafts[currentSpawned].cost, spawnableShafts[currentSpawned].type);
                hubHandler.takeResource(spawnableShafts[currentSpawned].cost2, spawnableShafts[currentSpawned].type2);
                currentSpawned += 1;
                spawned[currentSpawned-1].prefab.GetComponent<Shafts.Shaft>().ShowCavern();
            }
            else
            {
                if (unableToBuildShaft.IsValid())
                {
                    unableToBuildShaft.Post(gameObject);
                }
                //TODO blink missing resources or something.
            }
        }

        
    }

    public void AddNewShaft()
    {
        if (currentSpawned < spawnableShafts.Count)
        {
            shaftHandler.spawnNewShaft(spawnableShafts[currentSpawned].prefab, spawnableShafts[currentSpawned].size, shaftsParent,spawnableShafts[currentSpawned]);
        }
       
    }

    public Shaft GetCurrentShaft()
    {
        return spawnableShafts[currentSpawned];
    }

    public void updateShaft(GameObject o)
    {
        Shaft newShaft;
        newShaft = spawnableShafts[currentSpawned];
        newShaft.prefab = o;
        spawned.Add(newShaft);
    }

    public void setShaftsActive(bool set)
    {
        for(int i =0; i < spawned.Count; i++)
        {
            spawned[i].prefab.SetActive(set);
            shaftHandler.updateCamera(spawned[i].size);
        }
    }

    public int ObtainTotalCavernAmount()
    {
        return gameCaverns.Count;
    }

    public void ToggleEncounterState(bool state)
    {
        globalCanvasUI.ToggleEncounterState(!state);
    }

    public void setSpawnedTutorial(bool set)
    {
        spawnedShaftTutorial = set;
    }
    public bool getSpawnedTutorial()
    {
        return spawnedShaftTutorial;
    }

    public bool getCavernLoaded(Cavern c)
    {
        foreach (SpawnedCavern cavern in spawnedCaverns)
        {
            if (cavern.cavern == c)
            {
                Debug.Log("Cavern reloaded");
                return false;
               
            }
        }

        Debug.Log("NEW CAVERN ENTERED!");
        SpawnedCavern newSpawn = new SpawnedCavern();
        newSpawn.cavern = c;
        spawnedCaverns.Add(newSpawn);
        return true;
    }

    public void blockRaycasts()
    {
        blocker.SetActive(true);
    }

    public void hideBlocker()
    {
        Invoke("hide", 0.2f);
    }

    void hide()
    {
        blocker.SetActive(false);
    }
}
