﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NextDayStartButton : MonoBehaviour
{
    public void OnClick()
    {
        GameManager.Instance.dayManager.NextDayState();
    }
}

    
