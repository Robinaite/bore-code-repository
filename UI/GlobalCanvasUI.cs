﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GlobalCanvasUI : MonoBehaviour
{
    
    public GameObject nextdayBanner;
    [SerializeField]
    private TextMeshProUGUI dayCount;
    [SerializeField]
    private TextMeshProUGUI numberOfPeople;
    [SerializeField]
    private TextMeshProUGUI oreCount;
    [SerializeField]
    private TextMeshProUGUI woodCount;
    [SerializeField]
    private TextMeshProUGUI foodCount;
    [SerializeField]
    private Image SceneFader;
    [SerializeField]
    private GameObject cavernTransition;
    [SerializeField] 
    private GameObject SettingsPanel;
    [SerializeField] 
    private GameObject musicCreditsPanel;

    [System.Serializable]
    public struct CavernIntertiorData
    {
        [SerializeField]
        [ValueDropdown("GetAllSceneNames", NumberOfItemsBeforeEnablingSearch = 0)]
        public string name;
        [SerializeField]
        public Sprite sprite;

#if UNITY_EDITOR
        private static IEnumerable GetAllSceneNames()
        {

            List<string> sceneNames = new List<string>();

            foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            {
                sceneNames.Add(scene.path);
            }

            return sceneNames;
        }
#endif
    }
    [SerializeField]
    List<CavernIntertiorData> transitionInterior = new List<CavernIntertiorData>();

    [SerializeField]
    private GameObject encounterStateParent;
    
    private Action newDayAction;

    private HubHandler hubHandler;
    private DayManager dayManager;

    public delegate void Settings(bool state);

    public Settings OnSettingsStateChange;
    
    private void Start()
    {
        hubHandler = GameManager.Instance.hubHandler;
        dayManager = GameManager.Instance.dayManager;
        cavernTransition.SetActive(false);
    }

    public void ShowNextDayBanner(Action newDay)
    {
        newDayAction = newDay;
        dayCount.text = "Day "+ dayManager.GetCurrentDayNumber() + " Summary";
        numberOfPeople.text = GetNumberOfPeopleText();
        oreCount.text = ObtainResourceString(ResourceType.Types.ORE);
        woodCount.text = ObtainResourceString(ResourceType.Types.WOOD);
        foodCount.text = ObtainResourceString(ResourceType.Types.FOOD);
        
        nextdayBanner.SetActive(true);
    }

    private string GetNumberOfPeopleText()
    {
        int totalAmount = hubHandler.GetPeopleTotalAmount();
        int potentialAmount = dayManager.GetPotentialNewPeople();
        string textToShow = totalAmount.ToString();
        if (potentialAmount > 0)
        {
            textToShow += " <color=#6CBD69>(+" + potentialAmount + ")";
        }

        return textToShow;
    }

    private string ObtainResourceString(ResourceType.Types resourceType)
    {
        int totalAmount = hubHandler.ObtainResourceAmount(resourceType);
        int potentialAmount = dayManager.GetPotentialResource(resourceType);
        if (resourceType == ResourceType.Types.FOOD)
        {
            potentialAmount -= hubHandler.GetPeopleTotalAmount();
        }
        string textToShow = totalAmount.ToString();
        if (potentialAmount > 0)
        {
            textToShow += " <color=#6CBD69>(+" + potentialAmount + ")";
        }
        else if(potentialAmount < 0)
        {
            textToShow += " <color=#DC4932>(" + potentialAmount + ")";
        }

        return textToShow;
    }

    public void HideBanner()
    {
        nextdayBanner.SetActive(false);
        if (SceneManager.GetActiveScene().name != "hub")
        {
            GameManager.Instance.sceneLoader.OnFinishLoad += GoNextDay;
            
            GameManager.Instance.sceneLoader.LoadScene("EatingScene");
        }
        else
        {
            GoNextDay();
        }
    }

    private void GoNextDay()
    {
        GameManager.Instance.sceneLoader.OnFinishLoad -= GoNextDay;
    }

    public void afterEating()
    {
        newDayAction?.Invoke();
        newDayAction = null;
    }
    public bool IsShowingNextDayBanner()
    {
        return nextdayBanner.activeInHierarchy;
    }

    public void ShowMainMenu()
    {
        encounterStateParent.SetActive(false);
        GameManager.Instance.sceneLoader.LoadScene("StartGame");
    }


    public IEnumerator ShowSceneFader(string name)
    {
        ToggleSettingsScreen(false);
        SceneFader.gameObject.SetActive(true);
        Color color = SceneFader.color;
        do
        {
            SceneFader.color = Color.Lerp(SceneFader.color, new Color(color.r, color.g, color.b, 1), Time.deltaTime * 10f);
            yield return null;
        } while (SceneFader.color.a < 0.99);

        SceneFader.color = new Color(color.r, color.g, color.b, 1);

    }

    public IEnumerator HideSceneFader()
    {

        Color color = SceneFader.color;
        do
        {
            SceneFader.color = Color.Lerp(SceneFader.color, new Color(color.r, color.g, color.b, 0), Time.deltaTime * 10f);
            yield return null;
        } while (SceneFader.color.a > 0.01);

        SceneFader.gameObject.SetActive(false);
        SceneFader.color = new Color(color.r, color.g, color.b, 0);

    }

    public IEnumerator cavernTransitionLoop(string name)
    {
        Sprite s = transitionInterior.Find(x => x.name == name).sprite;

        if (s != null)
        {
            for (int i = 0; i < cavernTransition.transform.childCount; i++)
            {
                cavernTransition.transform.GetChild(i).GetComponent<Image>().sprite = s;
            }

            cavernTransition.SetActive(true);
            yield return new WaitForSecondsRealtime(1);
        }
    }

    public void hideCavernTransition()
    {
        cavernTransition.SetActive(false);
    }

    public bool showCavernTransition(string name)
    {
        Sprite s = transitionInterior.Find(x => x.name == name).sprite;

        if (s != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ToggleEncounterState(bool state)
    {      
        encounterStateParent.SetActive(state);
    }

    public bool IsInEncounterState()
    {
        return !encounterStateParent.activeInHierarchy;
    }

    public void ToggleSettingsScreen()
    {
        ToggleSettingsScreen(!SettingsPanel.gameObject.activeInHierarchy);
    }

    private void ToggleSettingsScreen(bool state)
    {
        OnSettingsStateChange?.Invoke(state);
        SettingsPanel.gameObject.SetActive(state);
        Time.timeScale = state ? 0 : 1;
    }
    
    public void OnMusicVolumeChanged(float volume)
    {
        AkSoundEngine.SetRTPCValue("MusicVolume", volume);
    }
    
    public void OnSFXVolumeChanged(float volume)
    {
        AkSoundEngine.SetRTPCValue("SFXVolume", volume);
    }

    public void ToggleMusicCredits()
    {
        musicCreditsPanel.SetActive(!musicCreditsPanel.activeInHierarchy);
    }
    
}
