﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradePanel : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject upgradePanel1;
    public GameObject upgradePanel2;

    public void ToggleUpgradePanel()
    {
        upgradePanel1.SetActive(!(upgradePanel1.activeInHierarchy || upgradePanel2.activeInHierarchy));
        upgradePanel2.SetActive(false);
        GameManager.Instance.ToggleEncounterState(upgradePanel1.activeInHierarchy);
    }

    public void TogglePanel1()
    {
        upgradePanel1.SetActive(!upgradePanel1.activeInHierarchy);
        upgradePanel2.SetActive(!upgradePanel1.activeInHierarchy);
    }
}
