﻿using System;
using System.Collections;
using System.Collections.Generic;
using InputSamples.Gestures;
using UnityEngine;
using UnityEngine.Playables;

public class MainMenuUI : MonoBehaviour
{

    public PlayableDirector launchGameDirector;
    public GameObject mainMenu;
    public GameObject introScene;
    public GameObject skipButton;
    public GameObject exitGameButton;
    
    private void Start()
    {
        GameManager.Instance.gestureController.Tapped += ShowSkipButton;
        launchGameDirector.stopped += ShowMainMenu;
        launchGameDirector.Play();
        GameManager.Instance.globalCanvasUI.OnSettingsStateChange += ToggleExitGameButton;
    }

    private void OnDestroy()
    {
        if (GameManager.Instance)
        {
            GameManager.Instance.globalCanvasUI.OnSettingsStateChange -= ToggleExitGameButton;
            GameManager.Instance.gestureController.Tapped -= ShowSkipButton;
        }
        launchGameDirector.stopped -= ShowMainMenu;
    }

    private void ShowMainMenu(PlayableDirector obj)
    {
        if (launchGameDirector == obj)
        {
            mainMenu.SetActive(true);
            introScene.SetActive(false);
            skipButton.SetActive(false);
        }
    }

    public void SkipCutscene()
    {
        launchGameDirector.Stop();
        skipButton.SetActive(false);
        GameManager.Instance.gestureController.Tapped -= ShowSkipButton;
        launchGameDirector.stopped -= ShowMainMenu;
    }

    private void ShowSkipButton(TapInput tapInput)
    {
        if (!skipButton.activeInHierarchy && launchGameDirector.state == PlayState.Playing)
        {
            skipButton.SetActive(true);
            StopAllCoroutines();
            StartCoroutine(HideSkipButton());
        }
    }

    private IEnumerator HideSkipButton()
    {
        yield return new WaitForSeconds(3f);
        skipButton.SetActive(false);
    }


    public void StartGame()
    {
        GameManager.Instance.StartGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ToggleExitGameButton(bool state)
    {
        
    }
}
