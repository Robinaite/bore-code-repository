﻿using System.Collections;
using System.Collections.Generic;
using Shafts;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
    HubHandler hub;

    [SerializeField]
    GameObject upgradeIndicator;
    
    public TextMeshProUGUI currentDayText;
    public GameObject nextDayStateButton;
    public RectTransform buildShaftButton;
    public Image buildShaftButtonImage;
    public Color buildColor;
    public Color notBuildColor;
    
    public TextMeshProUGUI buildShaftCostText1;
    public Image buildShaftCostImage1;
    public GameObject buildshaftCost2;
    public TextMeshProUGUI buildShaftCostText2;
    public Image buildShaftCostImage2;
    [SerializeField]
    private List<TypeToSprite> typeToSprites = new List<TypeToSprite>();
    [System.Serializable]
    protected struct TypeToSprite
    {
        public ResourceType.Types resourceType;
        public Sprite sprite;
    }
    
   
    
    public RectTransform canvas;
    public List<GameObject> hideIfInEncounter = new List<GameObject>();
    private bool isInEncounter = false;
    private void OnEnable()
    {
        hub = GameManager.Instance.hubHandler;
    }

    // Update is called once per frame
    void Update()
    {
        //Ensure the upgrade indicator is active when supposed to be 
        upgradeIndicator.SetActive(hub.getInUpgradeMenu());
        
        Day.DayState dayState = GameManager.Instance.dayManager.GetCurrentDayState();
        
        currentDayText.text = GameManager.Instance.dayManager.GetCurrentDayNumber().ToString();

        if (SceneManager.sceneCount > 1 && !isInEncounter)
        {
            foreach (var uiObjects in hideIfInEncounter)
            {
                uiObjects.SetActive(false);
            }

            isInEncounter = true;
        }
        else if(isInEncounter && SceneManager.sceneCount < 2)
        {
            foreach (var uiObjects in hideIfInEncounter)
            {
                uiObjects.SetActive(true);
            }

            isInEncounter = false;
        }

        if (!isInEncounter && GameManager.Instance.ObtainTotalCavernAmount() > 0 && !nextDayStateButton.activeInHierarchy && GameManager.Instance.dayManager.ArePeopleSendToCaves() && GameManager.Instance.dayManager.IsCurrentDayState(Day.DayState.Planning))
        {
            nextDayStateButton.SetActive(true);
        } else if (isInEncounter && nextDayStateButton.activeInHierarchy)
        {
            nextDayStateButton.SetActive(false);
        }

        if (GameManager.Instance.globalCanvasUI.IsShowingNextDayBanner())
        {
            nextDayStateButton.SetActive(false);
        }
    }

    public void ShowBuildShaftButton(Shaft shaft, Camera camera)
    {
        if (!GameManager.Instance.globalCanvasUI.IsShowingNextDayBanner() && !shaft.IsShowingCavern())
        {
            buildShaftButtonImage.color = notBuildColor;

            Vector2 posOnCanvas = camera.WorldToViewportPoint(shaft.transform.position) * canvas.rect.size;

            buildShaftButton.anchoredPosition = posOnCanvas;

            GameManager.Shaft currentShaft = GameManager.Instance.GetCurrentShaft();

            buildShaftCostText1.text = hub.checkAvaliableResource(currentShaft.cost, currentShaft.type) ? "<color=green>" : "<color=red>";
            buildShaftCostText1.text += "-" + currentShaft.cost;
            buildShaftCostImage1.sprite =
                typeToSprites.Find(type => type.resourceType == currentShaft.type).sprite;

            if (hub.checkAvaliableResource(currentShaft.cost, currentShaft.type))
            {
                buildShaftButtonImage.color = buildColor;
            }
            
            if (currentShaft.cost2 > 0)
            {
                buildshaftCost2.gameObject.SetActive(true);
                buildShaftCostText2.text = hub.checkAvaliableResource(currentShaft.cost2, currentShaft.type2) ? "<color=green>" : "<color=red>";
                buildShaftCostText2.text += "-" + currentShaft.cost2;
                buildShaftCostImage2.sprite =
                    typeToSprites.Find(type => type.resourceType == currentShaft.type2).sprite;

                if (!hub.checkAvaliableResource(currentShaft.cost2, currentShaft.type2))
                {
                    buildShaftButtonImage.color = notBuildColor;
                }
                
            }
            else
            {
                buildshaftCost2.gameObject.SetActive(false);
            }
            
            
            
            buildShaftButton.gameObject.SetActive(true);
        }
        else
        {
            buildShaftButton.gameObject.SetActive(false);
        }
    }

    public void HideBuildShaftButton()
    {
        buildShaftButton.gameObject.SetActive(false);
    }
    
    
}
