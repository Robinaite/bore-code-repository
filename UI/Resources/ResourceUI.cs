﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class ResourceUI : MonoBehaviour
{
    [Required]
    [SerializeField]
    private ResourceType resource;
    
    [SerializeField]
    private TextMeshProUGUI valueText;

    private HubHandler hubHandler;
    private DayManager dayManager;
    
    // Start is called before the first frame update
    void Start()
    {
        //Get the information of the current amount of this resource
        if (!resource)
        {
            Debug.LogError("No resource scriptable object attached to this UI",this);
            return;
        }

        hubHandler = GameManager.Instance.hubHandler;
        dayManager = GameManager.Instance.dayManager;
        

        int totalAmount = hubHandler.ObtainResourceAmount(resource.getType());
        int potentialAmount = dayManager.GetPotentialResource(resource.getType());
        if (resource.getType() == ResourceType.Types.FOOD)
        {
            potentialAmount -= hubHandler.GetPeopleTotalAmount();
        }
        string textToShow = totalAmount.ToString();
        if (potentialAmount > 0)
        {
            textToShow += " <color=#6CBD69>(+" + potentialAmount + ")";
        }
        else if(potentialAmount < 0)
        {
            textToShow += " <color=#DC4932>(" + potentialAmount + ")";
        }
        
        valueText.text = textToShow;
    }

    // Update is called once per frame
    void Update()
    {
        if (!resource)
        {
            return;
        }

        int totalAmount = hubHandler.ObtainResourceAmount(resource.getType());
        int potentialAmount = dayManager.GetPotentialResource(resource.getType());
        if (resource.getType() == ResourceType.Types.FOOD)
        {
            potentialAmount -= hubHandler.GetPeopleTotalAmount();
        }
        string textToShow = totalAmount.ToString();
        if (potentialAmount > 0)
        {
            textToShow += " <color=#6CBD69>(+" + potentialAmount + ")";
        }
        else if(potentialAmount < 0)
        {
            textToShow += " <color=#DC4932>(" + potentialAmount + ")";
        }
        valueText.text = textToShow;

      
    }
}
