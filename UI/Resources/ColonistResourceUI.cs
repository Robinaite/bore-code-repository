﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ColonistResourceUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI valueText;

    private HubHandler hubHandler;
    private DayManager dayManager;
    
    // Start is called before the first frame update
    void Start()
    {
        //Get the information of the current amount of this resource


        hubHandler = GameManager.Instance.hubHandler;
        dayManager = GameManager.Instance.dayManager;

        int totalAmount = hubHandler.GetPeopleTotalAmount(); //This should be fixed when changing the hubhandler
        int poolSize = dayManager.GetCurrentPeoplePoolSize();

        valueText.text = poolSize + "/" +totalAmount;;
    }

    // Update is called once per frame
    void Update()
    {
        int totalAmount = hubHandler.GetPeopleTotalAmount();
        int poolSize = dayManager.GetCurrentPeoplePoolSize();

        valueText.text = poolSize + "/" +totalAmount;;
    }
}
