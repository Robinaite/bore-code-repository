# What is Bore?

Bore is a game made for DES504 Module of the Professional Master in Games Development Course at Abertay University.

BORE begins with the escape of our main characters from the toxic wasteland above ground to the unknown depths below. 

You can play the full game here: https://courtneyspivey.itch.io/bore

# What is this repository?

This repository contains all the scripts created for the logic of the game for usage of Portfolio for the programmers in the team.
The game was developed with Unity 2019.4.12F1 and WWise as the audio middleware.

The code is licensed under MIT License. Check the License document for more information
