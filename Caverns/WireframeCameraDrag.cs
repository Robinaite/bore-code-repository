﻿using System;
using System.Collections;
using InputSamples.Gestures;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WireframeCameraDrag : MonoBehaviour
{

    private GestureController gestureController;
    [SerializeField]
    [Required]
    private Transform cameraTransform;
    
    [SerializeField]
    private float topThreshold = 0;
    [SerializeField]
    private float bottomThreshold = 0;
    [SerializeField]
    private float extraThresholdWhileDragging = 2;

    [SerializeField] 
    private float lerpDuration = 1;

    [SerializeField]
    private float draggableAmount = 100;

    [ShowInInspector]
    [ReadOnly]
    private float changeCamerHeight = 0;

    public WireframeMap wireframeMap;

    private float cameraPositionYOnPress = 0;
    private bool cameraPositionYOnPressSetup = false;

    private Coroutine lerpBackToThreshold;
    
    void Start()
    {
        gestureController = GameManager.Instance.gestureController;
        if (!cameraTransform)
        {
            cameraTransform = GetComponent<Transform>();
        }
        if (!GameManager.Instance.firstTimeSetup)
        {
            GameManager.Instance.cameraPositionSaved = transform.localPosition;
            GameManager.Instance.firstTimeSetup = true;
        }
        else
        {
            transform.localPosition = GameManager.Instance.cameraPositionSaved;
        }

        if (GameManager.Instance.getSpawnedTutorial())
        {
            gestureController.Pressed += PressedScreen;
            gestureController.DraggingScreen += DraggingScreen;
            gestureController.EndDrag += ReleasedScreen;
        }
        else
        {
            StartCoroutine(WaitAfterStartToSetupGestures());
        }
    }

    private IEnumerator WaitAfterStartToSetupGestures()
    {
        yield return new WaitForSeconds(0.5f);

        while (SceneManager.sceneCount > 1)
        {
            yield return null;
        } 
        gestureController.Pressed += PressedScreen;
        gestureController.DraggingScreen += DraggingScreen;
        gestureController.EndDrag += ReleasedScreen;
    }

    
    private void OnDestroy()
    {
        if (GameManager.Instance)
        {
            GameManager.Instance.cameraPositionSaved = cameraTransform.localPosition;
        }
        
        
        gestureController.Pressed -= PressedScreen;
        gestureController.DraggingScreen -= DraggingScreen;
        gestureController.EndDrag -= ReleasedScreen;
    }

    private void PressedScreen(SwipeInput swipeInput)
    {
        if (SceneManager.sceneCount > 2)
        {
            return;
        }
        
        cameraPositionYOnPressSetup = true;
        cameraPositionYOnPress = cameraTransform.localPosition.y;
        if (lerpBackToThreshold != null)
        {
            StopCoroutine(lerpBackToThreshold);
        }
        
    }

    private void DraggingScreen(SwipeInput swipeInput)
    {

        if (SceneManager.sceneCount > 2)
        {
            return;
        }
        
        wireframeMap.wireframeMapUI.HideBuildShaftButton();

        float startPosition = swipeInput.StartPosition.y;
        
        if (!cameraPositionYOnPressSetup)
        {
            cameraPositionYOnPress = cameraTransform.localPosition.y;
            cameraPositionYOnPressSetup = true;
            startPosition = swipeInput.PreviousPosition.y;
        }
        
        changeCamerHeight = swipeInput.PreviousPosition.y - startPosition;
        changeCamerHeight /= draggableAmount;
        
        if (swipeInput.SwipeVelocity == 0) //TODO, potentially use this as threshold for minimum
        {
            return;
        }

        if (!cameraPositionYOnPressSetup)
        {
            cameraPositionYOnPress = cameraTransform.localPosition.y;
            cameraPositionYOnPressSetup = true;
        }
        
        float newCamPosition = cameraPositionYOnPress - changeCamerHeight;

        if (newCamPosition > topThreshold + extraThresholdWhileDragging)
        {
            newCamPosition = topThreshold + extraThresholdWhileDragging;
        } else if (newCamPosition < bottomThreshold - extraThresholdWhileDragging)
        {
            newCamPosition = bottomThreshold - extraThresholdWhileDragging;
        }

        if (SceneManager.sceneCount == 1)
        {
            cameraTransform.localPosition = new Vector3(cameraTransform.localPosition.x,newCamPosition,cameraTransform.localPosition.z);
        }
    }


    private void ReleasedScreen(SwipeInput swipeInput)
    {
        if (SceneManager.sceneCount > 2)
        {
            return;
        }
        
        if (cameraTransform.localPosition.y > topThreshold)
        {
            lerpBackToThreshold = StartCoroutine(LerpToThreshold(topThreshold));
        } else if (cameraTransform.localPosition.y < bottomThreshold)
        {
            lerpBackToThreshold = StartCoroutine(LerpToThreshold(bottomThreshold));
        }
    }

    private IEnumerator LerpToThreshold(float thresholdVal)
    {
        float time = 0;
        float startPosition = cameraTransform.localPosition.y;

        float newCamPosition = 0;
        
        while (time < lerpDuration)
        {
            newCamPosition = Mathf.Lerp(startPosition, thresholdVal, time / lerpDuration);
            cameraTransform.localPosition = new Vector3(cameraTransform.localPosition.x,newCamPosition,cameraTransform.localPosition.z);
            time += Time.deltaTime;
            yield return null;
        }
        cameraTransform.localPosition = new Vector3(cameraTransform.localPosition.x,thresholdVal,cameraTransform.localPosition.z);
    }

    public void updateBottomThreshold(float amount)
    {
        bottomThreshold -= amount;
    }
}
