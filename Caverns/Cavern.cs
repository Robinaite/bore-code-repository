﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cavern
{
    private CavernData cavernData;
    private ResourceType.Types resourceType = ResourceType.Types.NONE;
    private int level = 0;
    private int upgradeCost = 1;

    private CavernWireframe cavernWireframe;

    public Cavern(CavernData cavernData, CavernWireframe cavernWireframe)
    {
        this.cavernData = cavernData;

        List<ResourceType> resourceTypes = cavernData.ObtainResourceTypes();
        if (resourceTypes.Count == 1)
        {
            resourceType = resourceTypes[0].getType();
        }
        
        this.cavernWireframe = cavernWireframe;
    }

    public int ObtainResources(int amountOfPeople)
    {
        return cavernData.ObtainResources(amountOfPeople,resourceType,level);
    }

    public ResourceType.Types GetResourceType()
    {
        return resourceType;
    }

    public bool IsOfCavernWireframe(CavernWireframe cavernWireframe)
    {
        return this.cavernWireframe == cavernWireframe;
    }

    public void SetResourceType(ResourceType.Types type)
    {
        resourceType = type;
    }

    public void UpgradeLevel()
    {
        level++;
        Debug.Log("Cavern upgraded!");
    }

    public int GetLevel()
    {
        return level;
    }

    public int getUpgradeCost()
    {
        return upgradeCost;
    }
    
    public int ObtainAmountOfPeopleRequiredToOpenCave()
    {
        return cavernData.ObtainAmountOfPeopleRequiredToOpenCave();
    }

    public void ToggleCavernWireframeEncounterAlert(bool state)
    {
        cavernWireframe.ToggleEncounterAlert(state);
    }

    public EncounterData getCavernTutorial()
    {
        return this.cavernWireframe.getTutorial();
    }
    
}
