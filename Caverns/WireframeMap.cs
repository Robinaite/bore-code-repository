﻿using System;
using System.Collections;
using System.Collections.Generic;
using InputSamples.Gestures;
using Shafts;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class WireframeMap : MonoBehaviour
{
    [SerializeField]
    [Required]
    private Camera camera;
    
    private const string CavernOnMapLayer = "CavernOnMap";

    private GestureController gestureController;

    [SerializeField]
    private GameObject peopleOnMap;

    [SerializeField] public MainUI wireframeMapUI;
    
    [SerializeField]
    private AK.Wwise.Event cavernClickEvent;

    private void Start()
    {
        if (!camera)
        {
            camera = Camera.main;
        }

        gestureController = GameManager.Instance.gestureController;
        
        gestureController.Tapped += OnCavernClick;

        if (GameManager.Instance.shaftsSpawnedCount == 0)
        {
            GameManager.Instance.AddNewShaft();
        }
    }

    private void OnEnable()
    {
        camera.transform.position = new Vector3(0, 7, -6);
    }
    private void OnDisable()
    {
        gestureController.Tapped -= OnCavernClick;
    }

    private void OnCavernClick(TapInput input)
    {
        if (SceneManager.sceneCount > 1 || GameManager.Instance.globalCanvasUI.IsShowingNextDayBanner() || EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        
        wireframeMapUI.HideBuildShaftButton();
        
        Ray rayFromScreen = camera.ScreenPointToRay(input.ReleasePosition); 

        if (Physics.Raycast(rayFromScreen, out RaycastHit hit, 100, LayerMask.GetMask(CavernOnMapLayer)))
        {
            CavernWireframe cavernWireframe = hit.collider.GetComponent<CavernWireframe>();
            if (cavernWireframe)
            {
                cavernClickEvent.Post(cavernWireframe.gameObject);
                cavernWireframe.OnCavernClick();
                
            }
            else
            {
                HubWireframe hubWireframe = hit.collider.GetComponent<HubWireframe>();
                if (hubWireframe)
                {
                    cavernClickEvent.Post(hubWireframe.gameObject);
                    hubWireframe.OnHubClick();
                }
                else
                {
                    Shaft shaft = hit.collider.GetComponent<Shaft>();
                    if (shaft)
                    {
                        cavernClickEvent.Post(shaft.gameObject);
                        wireframeMapUI.ShowBuildShaftButton(shaft,camera);
                    }
                }
            }
        }
    }
}
