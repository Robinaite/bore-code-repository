﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


[CreateAssetMenu(fileName = "CavernData",menuName = "CavernData",order = 0)]
public class CavernData : SerializedScriptableObject
{
    [SerializeField]
    private List<ResourceType> possibleCavernResources = new List<ResourceType>();

    [SerializeField]
    private int amountOfPeopleRequiredToOpenCave = 1;

    [SerializeField]
    [Tooltip("The cost of the materials to upgrade this cavern")]
    private int upgradeCost = 1;
    

    public int ObtainResources(int amountOfPeople,ResourceType.Types resourceType,int cavernUpgrade)
    {
        if (resourceType == ResourceType.Types.NONE || cavernUpgrade == 0)
        {
            return 0;
        }
        
        ResourceType cavernResource = possibleCavernResources.Find(resource => resource.getType() == resourceType);
        if (default(ResourceType) == cavernResource)
        {
            return 0;
        }
        
        return amountOfPeople * cavernResource.GetAmountPerColonist(cavernUpgrade);;
    }

    public bool IsOnlyOneType()
    {
        return possibleCavernResources.Count == 1;
    }

    public List<ResourceType> ObtainResourceTypes()
    {
        return possibleCavernResources;
    }

    public int ObtainAmountOfPeopleRequiredToOpenCave()
    {
        return amountOfPeopleRequiredToOpenCave;
    }
}
