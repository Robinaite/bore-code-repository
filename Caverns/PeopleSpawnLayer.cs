﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleSpawnLayer : MonoBehaviour
{
    [SerializeField]
    private List<Transform> spawnPoints = new List<Transform>();
    public ParralaxEffect parralaxEffect;
    public SpriteRenderer spriteRenderer;

    private int lastSpawnPointUsed = 0;

    public Vector3 GetSpawnPoint()
    {
        Vector3 position = spawnPoints[lastSpawnPointUsed].position;
        lastSpawnPointUsed++;
        if (lastSpawnPointUsed >= spawnPoints.Count)
        {
            lastSpawnPointUsed = 0;
        }

        return position;
    }

}
