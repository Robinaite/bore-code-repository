﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CavernInteriorUI : MonoBehaviour
{
    [SerializeField]
    [Required]
    public CavernInterior cavernInterior;
    [Required]
    public TextMeshProUGUI amountOfPeopleText;

    public TextMeshProUGUI peopleAMountTitle;

    private void Start()
    {
        if (cavernInterior.ObtainCavernLevel() == 0)
        {
            peopleAMountTitle.text = "Unlock Cavern";
        }
    }

    public void IncreasePeople()
    {
        cavernInterior.IncreaseAmountOfPeople();
    }

    public void DecreasePeople()
    {
        cavernInterior.DecreaseAmountOfPeople();
    }

    private void Update()
    {
        string amountOfPeoplestring = "";
        
        if (cavernInterior.amountOfPeople >= 0)
        {
            amountOfPeoplestring = cavernInterior.amountOfPeople.ToString();
        }
        else
        {
            amountOfPeoplestring = 0.ToString();
        }

        if (cavernInterior.ObtainCavernLevel() == 0)
        {
            amountOfPeoplestring += "/" + cavernInterior.ObtainAmountOfPeopleRequiredToOpenCave();
        }
        amountOfPeopleText.text = amountOfPeoplestring;
    }

    //Commented out as its not necessary for Alpha, as we won't need selection of resources.
    /*public void SetOre()
    {
        SetResourceType(ResourceType.Types.ORE);
    }
    
    public void SetWood()
    {
        SetResourceType(ResourceType.Types.WOOD);
    }
    public void SetFood()
    {
        SetResourceType(ResourceType.Types.FOOD);
    }

    private void SetResourceType(ResourceType.Types type)
    {
        cavernInterior.SetCavernType(type);
        SetResourceButtons.SetActive(false);
        GameManager.Instance.sceneLoader.LoadInteriorScene(cavernInterior.cavern);
    }*/

    public void ReturnToMap()
    {
        cavernInterior.ReturnBackToWireframe();
    }
}
