﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CavernInterior : MonoBehaviour
{
    public Cavern cavern { get; private set; }

    public int amountOfPeople { get; private set; } = 0;
    
    private DayManager dayManager;

    [SerializeField]
    [Tooltip("Where the encounter prefab alert can be spawned")]
    private List<Transform> encounterSpawnPoints = new List<Transform>();
    
    [SerializeField]
    private PeopleInCavern peoplePrefab;

    //Spawn point for adding new people to the hub
    [SerializeField]
    private List<Transform> peopleSpawnPoints = new List<Transform>();

    private int lastSpawnPointUsed = 0;
    
    Stack<PeopleInCavern> peopleSpawned = new Stack<PeopleInCavern>();

    private PeopleSpawnLayer spawnLayer;

    public bool IsSetup { get; private set; } = false;

    [SerializeField]
    public EncounterData cavernTutorial;
    SpawnedEncounter encounter;

    public void Setup(Cavern currentCavern, EncounterData tutorial)
    {
        cavern = currentCavern;
        dayManager = GameManager.Instance.dayManager;
        amountOfPeople = dayManager.GetAmountOfPeopleFromCavern(cavern);

        cavernTutorial = cavern.getCavernTutorial();

        //Get spawned encounter from dayCavern
        List<SpawnedEncounter> spawnedEncounters = dayManager.GetSpawnedEncountersOfCavern(cavern);
        
        //Foreach SpawnedEncounter create Prefab in Scene.
        foreach (var spawnedEncounter in spawnedEncounters)
        {
            if (!spawnedEncounter.IsEncounterDone())
            {
                GameObject buttonEncounterGO = Instantiate(spawnedEncounter.GetEncounterInteriorAlertPrefab(), encounterSpawnPoints[0]);
                buttonEncounterGO.GetComponent<Button>().onClick.AddListener(() => spawnedEncounter.StartEncounter(cavern,null));
            }
        }

        SpawnHubPeople();

        IsSetup = true;

        if (tutorial != null)
        {
            if (GameManager.Instance.getCavernLoaded(cavern))
            {
                encounter = new SpawnedEncounter(tutorial);
                encounter.StartEncounter(null, setTutorialCompleted, true);
            }
        }
        else
        {
            Debug.Log("CAVERN DOES NOT HAVE DIALOGUE DATA FOR A TUTORIAL!");
        }
    }

    void setTutorialCompleted()
    {
        GameManager.Instance.getCavernLoaded(cavern);
        Debug.Log("Encounter done!");
    }

    public void IncreaseAmountOfPeople()
    {
        if (!spawnLayer)
        {
            spawnLayer = FindObjectOfType<PeopleSpawnLayer>();
        }
        
        //Check first if enough people are present in the hub
        if (dayManager.AddPeopleToCavern(cavern))
        {
            amountOfPeople++;
            SpawnNewPeople(spawnLayer);
        }
    }

    public void DecreaseAmountOfPeople()
    {
        if (dayManager.RemovePeopleFromCavern(cavern))
        {
            amountOfPeople--;
            DestroyOneHubPeople();
        }
    }

    public void ReturnBackToWireframe()
    {
        //Load back into wireframe map
        GameManager.Instance.sceneLoader.LoadScene("WireframeMap");
    }

    public bool IsOfCavernType(ResourceType.Types type)
    {
        return cavern.GetResourceType() == type;
    }

    public void SetCavernType(ResourceType.Types type)
    {
        cavern.SetResourceType(type);
    }

    public int ObtainAmountOfPeopleRequiredToOpenCave()
    {
        return cavern.ObtainAmountOfPeopleRequiredToOpenCave();
    }

    public int ObtainCavernLevel()
    {
        return cavern.GetLevel();
    }

    
    private void SpawnHubPeople()
    {
        if (cavern.GetLevel() == 0)
        {
            return;
        }
        
        if (!spawnLayer)
        {
            spawnLayer = FindObjectOfType<PeopleSpawnLayer>();
        }
        
        for (int i = 0; i < amountOfPeople; i++)
        {
            SpawnNewPeople(spawnLayer);
        }
    }

    private void SpawnNewPeople(PeopleSpawnLayer spawnLayer)
    {
        if (cavern.GetLevel() == 0)
        {
            return;
        }
        
        PeopleInCavern peopleInCavern = Instantiate(peoplePrefab, spawnLayer.GetSpawnPoint(),
            Quaternion.identity,spawnLayer.transform);
        peopleInCavern.Setup(null,spawnLayer.spriteRenderer.sortingOrder);
        peopleSpawned.Push(peopleInCavern);

        lastSpawnPointUsed++;
        if (lastSpawnPointUsed >= peopleSpawnPoints.Count)
        {
            lastSpawnPointUsed = 0;
        }
    }

    private void DestroyOneHubPeople()
    {
        if (peopleSpawned.Count > 0)
        {
            PeopleInCavern peopleInCavern = peopleSpawned.Pop();
            Destroy(peopleInCavern.gameObject);
        }        
        
    }

}