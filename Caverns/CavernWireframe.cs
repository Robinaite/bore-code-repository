﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CavernWireframe : MonoBehaviour
{
    [SerializeField]
    private CavernData cavernData;
    
    HubHandler hubHandler;
    private Cavern cavern;
    [SerializeField]
    private GameObject encounterAlert;
    
    [SerializeField]
    private GameObject peopleToSpawn;
    [SerializeField]
    private List<Transform> peopleSpawnPoints = new List<Transform>();
    private int lastSpawnPointUsed = 0;
    private Stack<GameObject> peopleSpawned = new Stack<GameObject>();

    private bool firstSpawn = false;
    public Animator cavernSpawnAnimator;
    public List<GameObject> generalObjects;


    [SerializeField]
    EncounterData cavernTutorial;

    private void Awake()
    {
        firstSpawn = false;
    }

    private void Start()
    {
        hubHandler = GameManager.Instance.hubHandler;
        cavern = GameManager.Instance.CreateNewCavern(cavernData,this); //With the shafts being build, this method will only ever be called once per cavern. This works cause of how the shafts are currently permanent across all scenes.   
        
    }


    private void OnEnable()
    {
        GameManager.Instance.dayManager.newDayDel += SpawnPeople;
        SpawnPeople();
        if (!firstSpawn)
        {
            cavernSpawnAnimator.SetTrigger("CavernPopUp");
            firstSpawn = true;
        }
        else
        {
            foreach (var gameObj in generalObjects)
            {
                gameObj.SetActive(true);
            }
        }
    }

    private void OnDisable()
    {
        GameManager.Instance.dayManager.newDayDel -= SpawnPeople;
    }

    public void OnCavernClick()
    {
        if (!hubHandler.getInUpgradeMenu())
        {
            GameManager.Instance.sceneLoader.LoadInteriorScene(cavern);
        }
        else
        {
            GameManager.Instance.hubHandler.upgradeCavern(this);
            GameManager.Instance.sceneLoader.LoadScene(hubHandler.GetHubScenePath());
            hubHandler.setInUpgradeMenu(false);
        }
    }

    public void ToggleEncounterAlert(bool state)
    {
        if (state)
        {
            encounterAlert.SetActive(true);
        }
        else
        {
            encounterAlert.SetActive(false);
        }
    }
    
    public void SpawnPeople()
    {
        int peopleSpawnedCount = peopleSpawned.Count;
        for (int i = 0; i < peopleSpawnedCount; i++)
        {
            GameObject peopleInCavern = peopleSpawned.Pop();
            Destroy(peopleInCavern);
        }
        
        for (int i = 0; i < GameManager.Instance.dayManager.GetAmountOfPeopleFromCavern(cavern);i++)
        {
            peopleSpawned.Push(Instantiate(peopleToSpawn, peopleSpawnPoints[lastSpawnPointUsed]));
            lastSpawnPointUsed++;
            if (lastSpawnPointUsed >= peopleSpawnPoints.Count)
            {
                lastSpawnPointUsed = 0;
            }
        }
    }

    public EncounterData getTutorial()
    {
        return cavernTutorial;
    }
}
