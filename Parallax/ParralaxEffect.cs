﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ParralaxEffect : MonoBehaviour
{
    Vector2 length;
    Vector2 startPos;

    Vector2 mouseP;

    [SerializeField]
    public float parralaxEffect;

    MouseScript mouse;

    bool onAndroid = false;

    float Margin = 5f;
    float x;
    float y;
    float Easing = 0.5f;
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        length.x = GetComponent<SpriteRenderer>().bounds.size.x;
        length.y = GetComponent<SpriteRenderer>().bounds.size.y;

        pos = transform.position;

        mouse = Camera.main.GetComponent<MouseScript>();

        if(SystemInfo.supportsGyroscope && Application.platform != RuntimePlatform.WindowsEditor)
        {
            onAndroid = true;

            InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.EnableDevice(AttitudeSensor.current);
        }
        else
        {
            onAndroid = false;
        }

    }

    private void OnEnable()
    {
        pos = transform.position;

        mouse = Camera.main.GetComponent<MouseScript>();

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            onAndroid = false;
        }
        else
        {
            onAndroid = true;

            InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.EnableDevice(AttitudeSensor.current);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!onAndroid)
        {
            //Grab the mouse position 
            mouseP = mouse.getMouse();

            //Clamp the mouse position based on the screen size
            mouseP = new Vector2(Mathf.Clamp(mouseP.x, 0, Screen.width), Mathf.Clamp(mouseP.y, 0, Screen.height));

            //Calculate the distance
            Vector2 dist = (mouseP * parralaxEffect);

            //Adjust the distance, raw values are too big
            dist /= 2000;
            dist *= -1;

            //Update the sprite position
            transform.position = new Vector3(startPos.x + dist.x, startPos.y + dist.y, transform.position.z);

        }
        else
        {
            float targetX = -AttitudeSensor.current.attitude.ReadValue().x - Screen.width / 2f;
            float dx = targetX - x;
            x += dx * Easing;
            float targetY = -AttitudeSensor.current.attitude.ReadValue().y - Screen.height / 2f;
            float dy = targetY - y;
            y += dy * Easing;
            Vector3 direction = new Vector3(AttitudeSensor.current.attitude.ReadValue().y, -AttitudeSensor.current.attitude.ReadValue().x, 0f);

            direction *= 10;
            //transform.position = pos - direction / 30f * Margin * parralaxEffect;
            transform.position = Vector3.Lerp(transform.position, pos - direction / 30f * Margin * parralaxEffect, Time.deltaTime * 100);
        }

    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }


}
