﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class ParralaxCamera : MonoBehaviour
{
    Vector2 length;
    Vector2 startPos;

    [SerializeField]
    float parralaxEffect;

    GameObject cam;

    bool onAndroid = false;

    float Margin = 5f;
    float x;
    float y;
    float Easing = 0.5f;
    Vector3 pos;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    Vector3 originalPos;
    // Start is called before the first frame update
    void Start()
    {
        if (!spriteRenderer)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        originalPos = transform.position;
        startPos = transform.position;
        length.x = spriteRenderer.bounds.size.x;
        length.y = spriteRenderer.bounds.size.y;

        cam = Camera.main.gameObject;

        pos = transform.position;

        if (SystemInfo.supportsGyroscope)
        {
            onAndroid = true;

            InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.EnableDevice(AttitudeSensor.current);
        }
        else
        {
            onAndroid = false;
        }

        //if (Application.platform == RuntimePlatform.WindowsEditor)
        //{
        //    onAndroid = false;
        //}
        //else
        //{
        //    onAndroid = true;

        //    InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
        //    InputSystem.EnableDevice(AttitudeSensor.current);
        //}


    }

   
    
    // Update is called once per frame
    void Update()
    {
        if (onAndroid)
        {
            float targetX = -AttitudeSensor.current.attitude.ReadValue().x - Screen.width / 2f;
            float dx = targetX - x;
            x += dx * Easing;
            float targetY = -AttitudeSensor.current.attitude.ReadValue().y - Screen.height / 2f;
            float dy = targetY - y;
            y += dy * Easing;
            Vector3 direction = new Vector3(AttitudeSensor.current.attitude.ReadValue().y, -AttitudeSensor.current.attitude.ReadValue().x, 0f);

            direction *= 50;
            //transform.position = pos - direction / 30f * Margin * parralaxEffect;
            transform.position = Vector3.Lerp(transform.position, pos - direction / 30f * Margin * parralaxEffect, Time.deltaTime * 200);
        }
        else
        {
            Vector2 dist = cam.transform.position * parralaxEffect;
            //Update the sprite position
            transform.position = new Vector3(startPos.x + dist.x, startPos.y + dist.y, transform.position.z);
        }

    }

    private void OnEnable()
    {
        cam = Camera.main.gameObject;

        pos = originalPos;

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            onAndroid = false;
        }
        else
        {
            onAndroid = true;

            InputSystem.EnableDevice(UnityEngine.InputSystem.Gyroscope.current);
            InputSystem.EnableDevice(AttitudeSensor.current);
        }

    }
}
