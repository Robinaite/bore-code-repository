﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class MouseScript : MonoBehaviour
{
    //TEMPORARY: havn't got the whole input system under wraps yet, this is a quick implimentation
    Vector2 p;

    public void mousePos(InputAction.CallbackContext context)
    {
        p = context.ReadValue<Vector2>();
    }

    public Vector2 getMouse()
    {
        return p;
    }
}
