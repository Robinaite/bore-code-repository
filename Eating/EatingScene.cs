﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingScene : MonoBehaviour
{
    SceneLoader scene;
    HubHandler hub;
    GlobalCanvasUI UI;
    // Start is called before the first frame update
    void Start()
    {
        scene = GameManager.Instance.sceneLoader;
        hub = GameManager.Instance.hubHandler;
        UI = GameManager.Instance.globalCanvasUI;

        Invoke("loadHubScene", 3);
    }

    void loadHubScene()
    {
        scene.LoadScene(hub.GetHubScenePath());
        UI.afterEating();
    }
}
