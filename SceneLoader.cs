﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public static bool IsLoading { get; private set; } = false;
    
    public delegate void Loading();
    public Loading OnFinishLoad;
    public Loading OnFaderFinishLoading;
    
    public Loading OnSceneLoadBeforeSceneChanger;
    private Cavern currentCavern;
    
    private string previousScene = "";
    private bool previousSceneWasInterior = false;
    
    private GlobalCanvasUI canvasUI;

    //List of scenes based on Resource Type and upgrade level

    [System.Serializable]
    public struct CavernInteriorToScene
    {
        public ResourceType.Types type;
        public int upgradeLevel;
        [ValueDropdown("GetAllSceneNames",NumberOfItemsBeforeEnablingSearch = 0)]
        public string cavernScene;
        public EncounterData tutorial;

        public bool IsThisCavernType(Cavern cavern)
        {
            return type == cavern.GetResourceType() && cavern.GetLevel() == upgradeLevel;
        }
        
#if UNITY_EDITOR
        private static IEnumerable GetAllSceneNames()
        {
        
            List<string> sceneNames = new List<string>();

            foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            {
                sceneNames.Add(scene.path);
            }

            return sceneNames;
        }
#endif
    }
    
    [SerializeField]
    private List<CavernInteriorToScene> cavernInteriorToScenes = new List<CavernInteriorToScene>();

    [SerializeField]
    [Required]
    private GameObject cavernInteriorPrefab;


    EncounterData cavernTutorial;

    private void Start()
    {
        canvasUI = GameManager.Instance.globalCanvasUI;
    }

    private void LoadSceneWithPath(string scenePath, bool additive = false)
    {
        previousScene = SceneManager.GetActiveScene().path;
        
        StartCoroutine(additive ? LoadSceneAdditive(scenePath) : LoadSceneAsync(scenePath));
    }
    
    public void UnloadScene(string scenePath)
    {
        StartCoroutine(UnloadSceneAsync(scenePath));
    }

    public void LoadPreviousScene()
    {
        if (!string.IsNullOrEmpty(previousScene))
        {
            if (previousSceneWasInterior)
            {
                LoadInteriorScene(currentCavern);
                previousSceneWasInterior = false;
            }
            else
            {
                StartCoroutine(LoadSceneAsync(previousScene));
                previousScene = "";
            }
        }
    }
    
    private IEnumerator LoadSceneAsync(string scenePath)
    {
        IsLoading = true;
        //1. Start scene changer

        if (!canvasUI.showCavernTransition(scenePath))
        {
            yield return StartCoroutine(canvasUI.ShowSceneFader(scenePath));
        }
        else
        {
            yield return StartCoroutine(canvasUI.cavernTransitionLoop(scenePath));
        }

        OnFaderFinishLoading?.Invoke();
        
        //2. Load other scene.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        canvasUI.hideCavernTransition();
        OnSceneLoadBeforeSceneChanger?.Invoke();
        
        //3. Hide scene changer
        yield return StartCoroutine(canvasUI.HideSceneFader());
        
        OnFinishLoad?.Invoke();
        IsLoading = false;
    }

    private IEnumerator LoadSceneAdditive(string scenePath)
    {
        IsLoading = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Additive);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        
        OnFinishLoad?.Invoke();
        IsLoading = false;
    }
    
    private IEnumerator UnloadSceneAsync(string scenePath)
    {
        AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(scenePath);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        
        OnFinishLoad?.Invoke();
    }

    public void LoadInteriorScene(Cavern cavern)
    {
        previousSceneWasInterior = true;
        CavernInteriorToScene cavernInteriorToScene = cavernInteriorToScenes.Find(cav => cav.IsThisCavernType(cavern));

        if (!string.IsNullOrEmpty(cavernInteriorToScene.cavernScene))
        {
            currentCavern = cavern;
            
            cavernTutorial = cavernInteriorToScene.tutorial;
            Debug.Log("Scene Loader: " + cavernTutorial);
            OnSceneLoadBeforeSceneChanger += SetupInteriorScene;

            LoadSceneWithPath(cavernInteriorToScene.cavernScene);
        }
        else
        {
            Debug.Log("STRING IS EMPTY OR NULL");
        }

    }

    private void SetupInteriorScene()
    {
        OnSceneLoadBeforeSceneChanger -= SetupInteriorScene;

        //Instantiate cavern interior prefab
        GameObject interiorGO = Instantiate(cavernInteriorPrefab);
        interiorGO.GetComponent<CavernInterior>().cavernTutorial = cavernTutorial;
        //Get CavernInteriorScript
        CavernInterior cavernInterior = interiorGO.GetComponent<CavernInterior>();

        //Run setup method
        cavernInterior.Setup(currentCavern, cavernTutorial);
    }

    public void LoadScene(string scenePath, bool additive = false, bool prevWasInterior = false)
    {
        if (IsLoading)
        {
            StartCoroutine(WaitForOtherSceneToLoad(scenePath,additive,prevWasInterior));
            return;
        }
        previousSceneWasInterior = prevWasInterior;
        LoadSceneWithPath(scenePath,additive);
    }

    private IEnumerator WaitForOtherSceneToLoad(string scenePath, bool additive = false,bool prevWasInterior = false)
    {
        yield return new WaitUntil(() => !IsLoading);
        previousSceneWasInterior = prevWasInterior;
        LoadSceneWithPath(scenePath,additive);
    }


}
