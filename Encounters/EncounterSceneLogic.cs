﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EncounterSceneLogic : MonoBehaviour
{

    protected SpawnedEncounter spawnedEncounter;

    public virtual void StartEncounter(SpawnedEncounter encounterSpawned)
    {
        this.spawnedEncounter = encounterSpawned;
    }

    public virtual void FinishEncounter()
    {
        spawnedEncounter.FinishEncounter();
    }
    
}
