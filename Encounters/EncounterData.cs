﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(fileName = "EncounterData",menuName = "Encounters/Encounter Data",order = 0)]
public class EncounterData : SerializedScriptableObject
{
    [SerializeField]
    [InfoBox("This should be the same as the scene name of the cavern interior to be used.")]
    [ValueDropdown("GetAllSceneNames",NumberOfItemsBeforeEnablingSearch = 0)]
    [Required]
    private string encounterScenePath;
    [SerializeField]
    [InfoBox("If TRUE, loads the scene on top of current scene. Make sure to not have a camera in the scene as such.")]
    private bool loadSceneAdditively = false;

    [Tooltip("If Left empty will always validate true.")]
    [SerializeField] private BaseRequirement requirement;

    [SerializeField]
    [InfoBox("This is the prefab that gets spawned in the cavern interior if there is a spawned encounter in that scene")]
    [Required]
    private GameObject interiorAlertPrefab;

    [SerializeField]
    [InfoBox("At the end of the encounter go to next state automatically")]
    private bool nextDayStateOnFinish = false;

    [SerializeField]
    [InfoBox("Temporary!: set dialogue ID, if not dialogue encounter leave at 0")]
    public int dialogueID = 0;

    [SerializeField]
    [InfoBox("If another encounter is added here it will be executed after this encounter is finished.")]
    public EncounterData nextEncounter;

    [SerializeField]
    public bool goToMainMenu = false;
    
    [SerializeField]
    public bool isCavernEncounter = false;
    
    public bool ValidateEncounter(Cavern cavern)
    {
        if (!requirement)
        {
            return false;
        }

        if (goToMainMenu && requirement.ValidateRequirement(cavern))
        {
            
        }

        return requirement.ValidateRequirement(cavern);
    }

    public string GetEncounterScenePath()
    {
        return encounterScenePath;
    }

    public GameObject GetInteriorAlertPrefab()
    {
        return interiorAlertPrefab;
    }

    public bool LoadAdditively()
    {
        return loadSceneAdditively;
    }

    public bool GoNextDayStateOnFinish()
    {
        return nextDayStateOnFinish;
    }

#if UNITY_EDITOR
    private static IEnumerable GetAllSceneNames()
    {
        
        List<string> sceneNames = new List<string>();

        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            sceneNames.Add(scene.path);
        }

        return sceneNames;
    }
    #endif

}
