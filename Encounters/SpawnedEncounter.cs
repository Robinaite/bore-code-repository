﻿using System;
using System.Collections;
using UnityEngine;

public class SpawnedEncounter
{
    private EncounterData encounterData;
    private bool done = false;

    private string scenePath;
    private Cavern cavern;

    private SpawnedEncounter nextEncounter;

    private Action OnFinish;
    private bool loadHubSceneAtEnd = false;
    
    public SpawnedEncounter(EncounterData encData)
    {
        encounterData = encData;

        if (encounterData.nextEncounter)
        {
            nextEncounter = new SpawnedEncounter(encounterData.nextEncounter);
        }
    }

    public bool IsEncounterDone()
    {
        if (nextEncounter != null)
        {
            return done && nextEncounter.IsEncounterDone();
        }
        
        return done;
    }

    public GameObject GetEncounterInteriorAlertPrefab()
    {
        return encounterData.GetInteriorAlertPrefab();
    }

    /// <summary>
    /// This should only be called inside Cavern Interior scene when starting an encounter.
    /// </summary>
    public void StartEncounter(Cavern cavern, Action OnFinishAction, bool loadHubSceneAtEnd = false)
    {
        this.loadHubSceneAtEnd = loadHubSceneAtEnd;
        OnFinish = OnFinishAction;
        GameManager.Instance.ToggleEncounterState(true);
        SceneLoader loader = GameManager.Instance.sceneLoader;
        this.cavern = cavern;
        if (SceneLoader.IsLoading)
        {
            loader.StartCoroutine(WaitForOtherSceneToLoad(cavern));
        } else
        {
            loader.OnFinishLoad += StartEncounterAfterSceneLoad;
        
            loader.LoadScene(encounterData.GetEncounterScenePath(), encounterData.LoadAdditively(),cavern != null);

        }
       
    }

    private IEnumerator WaitForOtherSceneToLoad(Cavern cavern)
    {
        yield return new WaitUntil(() => !SceneLoader.IsLoading);
        GameManager.Instance.sceneLoader.OnFinishLoad += StartEncounterAfterSceneLoad;
        
        GameManager.Instance.sceneLoader.LoadScene(encounterData.GetEncounterScenePath(), encounterData.LoadAdditively(),cavern != null);

    }

    private void StartEncounterAfterSceneLoad()
    {
        GameManager.Instance.sceneLoader.OnFinishLoad -= StartEncounterAfterSceneLoad;
        
        //Find Encounter Game Logic.
        GameObject encounterGO = GameObject.FindGameObjectWithTag("EncounterLogic");

        EncounterSceneLogic encounterSceneLogic = encounterGO.GetComponent<EncounterSceneLogic>();
        encounterSceneLogic.StartEncounter(this);
    }
    
    /// <summary>
    /// This is called by the encounter scene logic script when finished.
    /// </summary>
    public void FinishEncounter()
    {
        done = true;
        GameManager.Instance.ToggleEncounterState(false);
        if (encounterData.GoNextDayStateOnFinish() )
        {
            GameManager.Instance.dayManager.NextDayState();
        } 

        cavern?.ToggleCavernWireframeEncounterAlert(false);

        if (encounterData.isCavernEncounter)
        {
            GameManager.Instance.sceneLoader.OnFinishLoad += GoToNextDay;
        }
        
        GameManager.Instance.sceneLoader.OnFinishLoad += ExecuteSavedAction;
        
        if (nextEncounter != null)
        {
            GameManager.Instance.sceneLoader.OnFinishLoad += StartNextEncounter;
        }
        
        
        if (encounterData.LoadAdditively())
        {
            GameManager.Instance.sceneLoader.UnloadScene(encounterData.GetEncounterScenePath());
        }
        else if (loadHubSceneAtEnd)
        {
            GameManager.Instance.sceneLoader.LoadScene("Hub");
        } else if (encounterData.goToMainMenu)
        {
            GameManager.Instance.ToggleEncounterState(true);
            GameManager.Instance.sceneLoader.LoadScene("StartGame");
        }
        else
        {
            GameManager.Instance.sceneLoader.LoadPreviousScene();
        }
    }

    private void StartNextEncounter()
    {
        GameManager.Instance.sceneLoader.OnFinishLoad -= StartNextEncounter;
        
        nextEncounter.StartEncounter(cavern,null);
    }
    
    public EncounterData getData()
    {
        return encounterData;
    }

    private void ExecuteSavedAction()
    {
        OnFinish?.Invoke();
        GameManager.Instance.sceneLoader.OnFinishLoad -= ExecuteSavedAction;
    }

    public Cavern GetCavern()
    {
        return cavern;
    }

    public void GoToNextDay()
    {
        GameManager.Instance.sceneLoader.OnFinishLoad -= GoToNextDay;
        GameManager.Instance.dayManager.NextDayState();
    }
    
}