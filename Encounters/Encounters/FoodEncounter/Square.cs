﻿using System;
using UnityEngine;

namespace Encounters.Encounters.FoodEncounter
{
    public class Square : MonoBehaviour
    {
        public SpriteRenderer squareRenderer;

        public Sprite cutSprite;
        public Sprite defaultSprite;

        [SerializeField]
        private AK.Wwise.Event cutCropEvent;
        
        public enum SquareState
        {
            Start,
            Passed,
            Current,
            Default
        }

        private void Awake()
        {
            SetSquareColor(SquareState.Default);
        }

        public void SetSquareColor(SquareState squareState)
        {
            switch (squareState)
            {
                case SquareState.Start:
                    cutCropEvent.Post(gameObject);
                    squareRenderer.sprite = cutSprite;
                    break;
                case SquareState.Passed:
                    cutCropEvent.Post(gameObject);
                    squareRenderer.sprite = cutSprite;
                    break;
                case SquareState.Current:
                    cutCropEvent.Post(gameObject);
                    squareRenderer.sprite = cutSprite;
                    break;
                case SquareState.Default:
                    squareRenderer.sprite = defaultSprite;
                    break;
                default:
                    squareRenderer.sprite = defaultSprite;
                    break;
            }
        }


    }
}
