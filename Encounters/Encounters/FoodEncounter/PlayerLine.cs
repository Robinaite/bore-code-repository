﻿using System;
using System.Collections.Generic;
using System.Linq;
using InputSamples.Gestures;
using UnityEngine;

namespace Encounters.Encounters.FoodEncounter
{
    public class PlayerLine : MonoBehaviour
    {

        private List<Square> squaresPassed = new List<Square>();
        [SerializeField]
        private FoodEncounter foodEncounter;
        [SerializeField]
        private Camera camera;
        
        private static float TOLERANCE = 0.01f;
        private Square currentSquare;
        [SerializeField]
        private SpriteRenderer spriteRenderer;

        private void Start()
        {
            GameManager.Instance.gestureController.Pressed += Pressed;
            GameManager.Instance.gestureController.DraggingScreen += Dragging;
            GameManager.Instance.gestureController.Release += Released;
        }

        private void OnDestroy()
        {
            GameManager.Instance.gestureController.Pressed -= Pressed;
            GameManager.Instance.gestureController.DraggingScreen -= Dragging;
            GameManager.Instance.gestureController.Release -= Released;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Square newSquare = other.gameObject.GetComponent<Square>();
            if (!newSquare)
            {
                return;
            }
            
            foodEncounter.AddSquare(newSquare);
            
            if (currentSquare)
            {
                //need to check if either Y or X is equal of the new square, as no diagonal allowed
                if (Math.Abs(newSquare.transform.position.y - currentSquare.transform.position.y) < TOLERANCE ||
                    Math.Abs(newSquare.transform.position.x - currentSquare.transform.position.x) < TOLERANCE)
                {

                    if (currentSquare == newSquare || squaresPassed.Contains(newSquare) || Vector3.Distance(newSquare.transform.localPosition,currentSquare.transform.localPosition) > 3)
                    {
                        return;
                    }

                    if (squaresPassed.Count == 1)
                    {
                        currentSquare.SetSquareColor(Square.SquareState.Start);
                    }
                    else
                    {
                        currentSquare.SetSquareColor(Square.SquareState.Passed);
                    }
                    
                    newSquare.GetComponent<Square>().SetSquareColor(Square.SquareState.Current);
                    currentSquare = newSquare;
                    squaresPassed.Add(currentSquare);
                    foodEncounter.UpdateCompleteText(squaresPassed.Count);
                }
                else
                {
                    //somehow connected a weird ass square which it shouldn't as such, u lose.
                    Debug.Log("Moved to weird square!");
                    //Finish();
                }
            }
            else
            {
                currentSquare = newSquare;
                currentSquare.SetSquareColor(Square.SquareState.Start);
                squaresPassed.Add(currentSquare);
                foodEncounter.UpdateCompleteText(squaresPassed.Count);
            }
        }

        

        private void OnTriggerExit2D(Collider2D other)
        {
            if (currentSquare == other.gameObject.GetComponent<Square>())
            {
                //Moved finger outside of the touch area, stop the minigame.
                Debug.Log("Moved outside of the square area !");
                //Finish();
            }
        }
        
        private void Finish()
        {
            spriteRenderer.enabled = false;
            spriteRenderer.transform.position = camera.ScreenToWorldPoint(Vector3.zero);
            if (squaresPassed.Count > 0)
            {
                squaresPassed[squaresPassed.Count-1].GetComponent<Square>().SetSquareColor(Square.SquareState.Start);
            }
            
            List<Square> squares = new List<Square>(squaresPassed);
            squaresPassed.Clear();
            currentSquare = null;
            foodEncounter.FinishedGame(squares.Count);
        }


        private void Pressed(SwipeInput input)
        {
            if (foodEncounter.playersTurn)
            {
                spriteRenderer.enabled = true;
                this.transform.position = PositionWithZAt0(camera.ScreenToWorldPoint(input.StartPosition));
            }
        }

        private void Dragging(SwipeInput input)
        {
            if (foodEncounter.playersTurn)
            {
                this.transform.position = PositionWithZAt0(camera.ScreenToWorldPoint(input.PreviousPosition));
            }
        }

        private void Released(SwipeInput input)
        {
            if (foodEncounter.playersTurn)
            {
                this.transform.position = PositionWithZAt0(camera.ScreenToWorldPoint(input.EndPosition));
                Finish();
            }
        }

        private Vector3 PositionWithZAt0(Vector3 toTransform)
        {
            return new Vector3(toTransform.x,toTransform.y,0);
        }
    }
}
