﻿using System.Collections.Generic;
using InputSamples.Gestures;
using TMPro;
using UnityEngine;

namespace Encounters.Encounters.FoodEncounter
{
    public class FoodEncounter : EncounterSceneLogic
    {

        [SerializeField]
        private int totalSquares = 4;

        [SerializeField]
        private GameObject startGameUI;
        [SerializeField]
        private GameObject endScoreUI;
        [SerializeField]
        private TextMeshProUGUI endScoreText;
        [SerializeField]
        private TextMeshProUGUI currentScoreText;
        [SerializeField]
        private GameObject tutorialFinger;

        private HashSet<Square> squares = new HashSet<Square>();
        private float multiplier = 2.0f;

        [SerializeField]
        private AK.Wwise.Event winEvent;
        [SerializeField]
        private AK.Wwise.Event loseEvent;

        public bool playersTurn { get; private set; } = false;
        
        public override void StartEncounter(SpawnedEncounter encounterSpawned)
        {
            base.StartEncounter(encounterSpawned);
            startGameUI.SetActive(true);
            tutorialFinger.SetActive(true);
            foreach (var square in squares)
            {
                square.SetSquareColor(Square.SquareState.Default);
            }
            squares.Clear();
            GameManager.Instance.gestureController.Pressed += StartGame;
        }

        public override void FinishEncounter()
        {
            endScoreUI.SetActive(false);
            GameManager.Instance.dayManager.SetCavernMultiplier(spawnedEncounter.GetCavern(),multiplier);
            base.FinishEncounter();
        }

        public void StartGame(SwipeInput swipeInput)
        {
            GameManager.Instance.gestureController.Pressed -= StartGame;
            
            
            endScoreUI.SetActive(false);
            tutorialFinger.SetActive(false);
            startGameUI.SetActive(false);
            UpdateCompleteText(0);
            playersTurn = true;
        }

        public void RetryGame()
        {
            foreach (var square in squares)
            {
                square.SetSquareColor(Square.SquareState.Default);
            }
            squares.Clear();
            endScoreUI.SetActive(false);
            startGameUI.SetActive(true);
            tutorialFinger.SetActive(true);
            GameManager.Instance.gestureController.Pressed += StartGame;
        }

        
        public void FinishedGame(int squaresCount)
        {
            playersTurn = false;
            float percSquaresHit = squaresCount/ (float)totalSquares;

            if (percSquaresHit < 0.1)
            {
                //multiplier is 0.2
                multiplier = 0.2f;
                loseEvent.Post(gameObject);
            } else if (percSquaresHit < 0.8)
            {
                multiplier = 0.8f;
                loseEvent.Post(gameObject);
            }
            else
            {
                winEvent.Post(gameObject);
                multiplier = 1.5f;
            }

            endScoreText.text = Mathf.RoundToInt(Mathf.Clamp((squaresCount / (float) totalSquares)*100, 0, 100)) + "% Complete";
            endScoreUI.SetActive(true);
        }

        public void UpdateCompleteText(int currentSquareAmount)
        {
            int percSquaresHit = Mathf.RoundToInt(Mathf.Clamp((currentSquareAmount / (float) totalSquares)*100, 0, 100));

            currentScoreText.text = percSquaresHit + "% Complete";
        }

        public void AddSquare(Square square)
        {
            squares.Add(square);
        }
    }
}
