﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Encounters.Encounters.CutsceneEncounter
{
    public class CutsceneEncounterLogic : EncounterSceneLogic
    {
        public PlayableDirector cutscene;

        public override void StartEncounter(SpawnedEncounter encounterSpawned)
        {
            base.StartEncounter(encounterSpawned);
            cutscene.stopped += FinishedCutscene;
            cutscene.Play();
        }

        private void FinishedCutscene(PlayableDirector obj)
        {
            if (cutscene == obj)
            {
                FinishEncounter();
            }
        }

        public override void FinishEncounter()
        {
            base.FinishEncounter();
        }

    }
}
