﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterButtonClickLogic : EncounterSceneLogic
{
    public override void StartEncounter(SpawnedEncounter encounterSpawned)
    {
        base.StartEncounter(encounterSpawned); //This should always be the FIRST line of this method
        
        Debug.Log("Encounter started");
    }

    public override void FinishEncounter()
    {
        Debug.Log("Encounter finished");
        
        base.FinishEncounter(); //This should always be the LAST line of this method.
    }

    public void Win()
    {
        FinishEncounter();
    }
}
