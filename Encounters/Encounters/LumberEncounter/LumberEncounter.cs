﻿using System.Collections;
using System.Collections.Generic;
using InputSamples.Gestures;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class LumberEncounter : EncounterSceneLogic
{
    //Gestures
    private GestureController gestureController;

    //Store all the intersections by the player in a list for later
    //only store the amount needed at the time (2 for the first round, 3 for the second, 4 for the third)
    [SerializeField]
    List<Vector3> playerIntersections = new List<Vector3>();

    //List of the produced interestions for the game
    [SerializeField]
    List<Vector3> pattern1 = new List<Vector3>();

    [SerializeField]
    List<Vector3> pattern2 = new List<Vector3>();

    [SerializeField]
    List<Vector3> pattern3 = new List<Vector3>();

    [SerializeField]
    List<Vector3> currentPattern = new List<Vector3>();

    [SerializeField]
    List<Sprite> woodSprites = new List<Sprite>();

    //Variables to store the left and right side of the lumber - these are based off of the wood sprite
    float leftSide = 0;
    float rightSide = 0;

    //Tracker for which round the player is currently on
    int currentRound = 1;

    //Max amount of hits on each side
    int maxIntersectionsLeft = 0;
    int maxIntersectionsRight = 0;

    //Store the max hits for when resetting the drag
    int storedMaxLeft = 0;
    int storedMaxRight = 0;

    //Objects that the indicator collides with, is positioned at the edges of the lumber sprite
    [SerializeField]
    GameObject left;

    [SerializeField]
    GameObject right;

    //How close the player has to be to the marker in order to count the value
    [SerializeField]
    
    [Range(0, 10)]
    [InfoBox("This value controls how far away the player can hit the sides while still being counted")]
    float topThreshold = 0;

    [SerializeField]
    [Range(0, 10)]
    [InfoBox("This value controls how close the player can be to get 100% on that specific line")]
    float bottomThreshold = 0;

    [SerializeField]
    [Range(0.5f, 3)]
    [InfoBox("This value controls how long the pattern is shown for")]
    float timePatternActive = 0;

    //Score text on screen
    [SerializeField]
    TextMeshProUGUI scoreText;

    int pointsHit = 0;
    int overallHit = 0;
    int maxLines = 0;

    Vector3 lineDrawn;
    Vector3 startDrawn;
    Vector3 endDrawn;

    [SerializeField]
    LineRenderer drawnLine;

    float finalScore = 0;
    int totalLinesDrawn = 0;

    [SerializeField]
    GameObject endScreen;

    [SerializeField]
    GameObject startingScreen;

    [SerializeField]
    TextMeshProUGUI finalMultiplyer;

    [SerializeField]
    TextMeshProUGUI gameStateText;  

    bool inMenu = false;

    Vector3 swipePosition;
    Vector3 swipeStart;
    bool isDragging = false;

    [SerializeField]
    GameObject woodParent;

    [SerializeField]
    GameObject blackout;

    GameObject newWood;

    [SerializeField]
    List<Sprite> lineSprites = new List<Sprite>();

    [SerializeField]
    GameObject lineObj;

    [SerializeField]
    TextMeshProUGUI progressIndicator;

    float previousScore = 0;


    [SerializeField]
    private GameObject tutorialFinger;
    
    [SerializeField]
    private AK.Wwise.Event winEvent;
    [SerializeField]
    private AK.Wwise.Event loseEvent;
    [SerializeField]
    private AK.Wwise.Event woodCutEvent;

    // Start is called before the first frame update
    void Start()
    {
        gestureController = GameManager.Instance.gestureController;

        endScreen.SetActive(false);
        lineObj.SetActive(false);

    }

    public override void StartEncounter(SpawnedEncounter encounterSpawned)
    {
        base.StartEncounter(encounterSpawned);
        gestureController.Pressed += PressedScreen;
        gestureController.DraggingScreen += DraggingScreen;
        gestureController.EndDrag += ReleasedScreen;
        gestureController.Swiped += SwipedScreen;

     
        drawnLine.gameObject.SetActive(false);
        lineObj.SetActive(false);

        inMenu = true;
        gameStateText.text = "";
        showStartScreen(false);
        

        blackout.GetComponent<Animator>().SetTrigger("Show");
    }

    private void OnDestroy()
    {
        gestureController.Pressed -= PressedScreen;
        gestureController.DraggingScreen -= DraggingScreen;
        gestureController.EndDrag -= ReleasedScreen;
        gestureController.Swiped -= SwipedScreen;
    }

    void PressedScreen(SwipeInput swipe)
    {
        Vector2 pos = swipe.StartPosition;
        ToggleTutorialFinger(false);
    }

    void ReleasedScreen(SwipeInput release)
    {
        Vector2 lastPos = release.EndPosition;
        resetDrag();
    }

    void DraggingScreen(SwipeInput drag)
    {
        swipeStart = Camera.main.ScreenToWorldPoint(new Vector3(drag.StartPosition.x, drag.StartPosition.y, 0));

        swipePosition = Camera.main.ScreenToWorldPoint(new Vector3(drag.EndPosition.x, drag.EndPosition.y, 0));

        swipePosition.z = 0;
        swipeStart.z = 0;
        isDragging = true;
    }
    void SwipedScreen(SwipeInput swipe)
    {
        if (!inMenu)
        {
            CancelInvoke();

            //Convert the screen space touches to world point
            startDrawn = Camera.main.ScreenToWorldPoint(new Vector3(swipe.StartPosition.x, swipe.StartPosition.y, -0.2f));
            endDrawn = Camera.main.ScreenToWorldPoint(new Vector3(swipe.EndPosition.x, swipe.EndPosition.y, -0.2f));

            //Set the z axis to be 0
            startDrawn.z = -0.2f;
            endDrawn.z = -0.2f;

            //Calculate the line direction
            lineDrawn = endDrawn - startDrawn;

            //Set the line renderer
            drawnLine.SetPosition(0, new Vector3(startDrawn.x, startDrawn.y, -0.2f));
            drawnLine.SetPosition(1, new Vector3(endDrawn.x, endDrawn.y,-0.2f));
            woodCutEvent.Post(gameObject);
            //Check the intersections of this like
            checkIntersections();

            //Show the line 
            drawnLine.gameObject.SetActive(true);
        }


        //Hide the line 
        Invoke("hideDrawnLine", 0.2f);
    }

    private void Update()
    {
        if(isDragging)
        {
            drawnLine.gameObject.SetActive(true);
            drawnLine.SetPosition(0, new Vector3(swipeStart.x, swipeStart.y, -0.2f));
            drawnLine.SetPosition(1, new Vector3(swipePosition.x, swipePosition.y, -0.2f));
        }
    }
    void hideDrawnLine()
    {
        drawnLine.gameObject.SetActive(false);
    }

    void checkIntersections()
    {
        //Raycast between the intersection points
        RaycastHit2D[] hits = Physics2D.RaycastAll(startDrawn, lineDrawn, lineDrawn.magnitude);

        //Ensure there is at least 2 hits before checking
        if (hits.Length > 1)
        {
            playerIntersections.Add(hits[0].point);

            playerIntersections.Add(hits[1].point);

            pointsHit += 2;

            checkScore();
        }
    }

    void setRound(int r)
    {


        //Max 3 rounds, once completed end the game
        if (r <= 3)
        {
            newWood = Instantiate(woodParent);

            //Set up the left and right bounds based on the sprite size
            leftSide = lineObj.transform.position.x - lineObj.GetComponent<Renderer>().bounds.size.x / 2;
            rightSide = lineObj.transform.position.x + lineObj.GetComponent<Renderer>().bounds.size.x / 2;

            rightSide -= 0.5f;
            leftSide += 0.5f;

            //Position the egdes at the sides to show the player the bounds
            right.transform.position = new Vector2(rightSide, right.transform.position.y);
            left.transform.position = new Vector2(leftSide, left.transform.position.y);
            bool showTutorialFinger = false;
            switch (r)
            {
                case 1:
                    maxLines = 1;
                    newWood.GetComponent<SpriteRenderer>().sprite = woodSprites[0];
                    currentPattern = new List<Vector3>(pattern1);
                    showTutorialFinger = true;
                    break;
                case 2:
                    maxLines = 2;
                    newWood.GetComponent<SpriteRenderer>().sprite = woodSprites[1];
                    currentPattern = new List<Vector3>(pattern2);
                    break;
                case 3:
                    maxLines = 3;
                    newWood.GetComponent<SpriteRenderer>().sprite = woodSprites[2];
                    currentPattern = new List<Vector3>(pattern3);
                    break;
                default:
                    maxLines = 2;
                    newWood.GetComponent<SpriteRenderer>().sprite = woodSprites[1];
                    currentPattern = new List<Vector3>(pattern2);
                    break;
            }

            Invoke("showLine", 1f);
            inMenu = true;
            gameStateText.text = "MEMORISE THE PATTERN!";
            blackout.GetComponent<Animator>().ResetTrigger("Hide");
            blackout.GetComponent<Animator>().SetTrigger("Show");
            Invoke("hidePattern", timePatternActive);
            
        }
        else
        {
            endLevel();
        }
    }

    private void ToggleTutorialFinger(bool state)
    {
        tutorialFinger.SetActive(state);
    }

    void showLine()
    {
        lineObj.SetActive(true);
        lineObj.GetComponent<SpriteRenderer>().sprite = lineSprites[currentRound - 1];
    }

    void hidePattern()
    {
        ToggleTutorialFinger(currentRound == 1);
        lineObj.SetActive(false);
        inMenu = false;
        gameStateText.text = "DRAW THE PATTERN YOU JUST SAW!";
        blackout.GetComponent<Animator>().ResetTrigger("Show");
        blackout.GetComponent<Animator>().SetTrigger("Hide");
    }

    void checkScore()
    {
        if (pointsHit == 2)
        {
            float total = 0;
            

            for (int i = 0; i < playerIntersections.Count; i++)
            {
                float d = playerIntersections[i].y - currentPattern[i].y;
                total += d;
            }

            if(total < 0)
            {
                total *= -1;
            }
            Debug.Log(total);
            applyScore(total);
            overallHit += 1;
            resetDrag();


            currentPattern.RemoveAt(0);
            
        }

        //Keep track of how many lines the player has drawn
        if(overallHit >= maxLines)
        {
            newWood.GetComponent<Animator>().SetTrigger("Exit");

            Invoke("nextWood", 0.5f);
        }
    }

    void nextWood()
    {
        clearLevel();
        currentRound += 1;
        setRound(currentRound);
    }
    void resetDrag()
    {
        //Once the player stops dragging, reset the values
        playerIntersections.Clear();
        maxIntersectionsLeft = storedMaxLeft;
        maxIntersectionsRight = storedMaxRight;
        pointsHit = 0;
        swipePosition = new Vector3(0, 0, 0);
        swipeStart = new Vector3(0, 0, 0);
        isDragging = false;
        drawnLine.gameObject.SetActive(false);
    }


    void applyScore(float d)
    {
        //Within the range of reaching 100%
        if (d <= bottomThreshold)
        {
            d = 100;
        }
        //Get whether the distance is within the thresholds
        else if (d <= topThreshold)
        {
            //Flip the percentage
            d = topThreshold - d;
            d *= 10;
        }
        else
        {
            d = 0;
        }

        totalLinesDrawn += 1;

        //Round the number to be clearer
        d = Mathf.RoundToInt(d);
        d = Mathf.Clamp(d, 0, 34 / maxLines);
      
        finalScore += d;

        finalScore = Mathf.Clamp(finalScore, 0, 100);
        progressIndicator.text = finalScore + "%";
    }

    void clearLevel()
    {
        playerIntersections.Clear();
        overallHit = 0;
        pointsHit = 0;
    }

    void endLevel()
    {
        woodParent.GetComponent<Animator>().SetTrigger("Exit");

        scoreText.text = finalScore + "%";

        if (finalScore < 80)
        {
            loseEvent.Post(gameObject);
        }
        else
        {
            winEvent.Post(gameObject);
        }

        endScreen.SetActive(true);
        inMenu = true;
    }

    public void retry()
    {
        endScreen.SetActive(false);
        maxLines = 0;
        totalLinesDrawn = 0;
        clearLevel();
        currentRound = 1;
        setRound(currentRound);
        inMenu = false;
        previousScore = 0;
        finalScore = 0;

        progressIndicator.text = 0 + "/100%";

        //Reset the wood animation
        woodParent.SetActive(false);
        woodParent.SetActive(true);

        blackout.SetActive(false);
        blackout.SetActive(true);
    }

    public void showStartScreen(bool set)
    {
        startingScreen.SetActive(set);

        if(!set)
        {
            inMenu = false;
            setRound(currentRound);
        }
    }
}
