﻿using System;
using UnityEngine;

namespace Encounters.Encounters.Day1Fight
{
    public class ShieldCollider : MonoBehaviour
    {
        public AK.Wwise.Event blockedFireballEvent;
        public AK.Wwise.Event blockedEnergyEvent;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Fireball>())
            {
                if (blockedFireballEvent.IsValid())
                {
                    AkSoundEngine.PostEvent(blockedFireballEvent.Id, gameObject);
                }
            }
            else if (other.GetComponent<EnergyBall>())
            {
                if (blockedEnergyEvent.IsValid())
                {
                    AkSoundEngine.PostEvent(blockedEnergyEvent.Id, gameObject);
                }
            }

            Destroy(other.gameObject);
        }
    }
}
