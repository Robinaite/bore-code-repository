﻿using System;
using TMPro;
using UnityEngine;

namespace Encounters.Encounters.Day1Fight
{
    public class EnergyBall : MonoBehaviour
    {

        public Day1FightEncounter encounterLogic;
        private bool hasTarget = false;
        private Vector3 target;
        
        [SerializeField]
        private float speed = 10f;

        public Rigidbody2D rigidBody;

        private Vector2 direction;
        
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (hasTarget)
            {
                
                rigidBody.MovePosition(rigidBody.position + direction * speed * Time.deltaTime);
            }
        }

        public void Fire(Vector2 target)
        {
            hasTarget = true;
            this.target = target;
            direction = (target - rigidBody.position).normalized;
        }

        private void OnDestroy()
        {
            encounterLogic.RemoveEnergyBall(this);
        }
    }
}
