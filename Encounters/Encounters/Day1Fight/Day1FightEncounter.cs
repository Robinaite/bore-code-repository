﻿using System;
using System.Collections;
using System.Collections.Generic;
using Encounters.Encounters.Day1Fight;
using InputSamples.Gestures;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Day1FightEncounter : EncounterSceneLogic
{
    [BoxGroup("Fireballs")]
    [MinMaxSlider(0f,10f)]
    public Vector2 fireballSpawnRate;
    [BoxGroup("Fireballs")]
    public GameObject fireballPrefab;

    [BoxGroup("Fireballs")] 
    public float spawnRadius = 5f;
    
    [BoxGroup("EnergyBalls")]
    [MinMaxSlider(0f,10f)]
    public Vector2 energyballSpawnRate;
    [BoxGroup("EnergyBalls")]
    public GameObject energyPrefab;

    [BoxGroup("EnergyBalls")] 
    public float energySpawnRadius = 5f;

    public Core core;
    public Shield shield;
    private Vector2 corePos;

    [BoxGroup("UI")]
    public GameObject retryButton;
    [BoxGroup("UI")]
    public GameObject winButton;
    [BoxGroup("UI")]
    public CanvasGroup specialTextCanvasGroup;
    [BoxGroup("UI")]
    public TextMeshProUGUI specialTextScreen;
    [BoxGroup("UI")] 
    public GameObject startScreen;
    [BoxGroup("UI")] 
    public GameObject tutorialFinger;
    [BoxGroup("Audio")]
    public AK.Wwise.Event winEvent;
    [BoxGroup("Audio")]
    public AK.Wwise.Event loseEvent;
    
    private HashSet<Fireball> fireballsInGame = new HashSet<Fireball>();
    private HashSet<EnergyBall> energyballsInGame = new HashSet<EnergyBall>();

    public override void StartEncounter(SpawnedEncounter encounterSpawned)
    {
        base.StartEncounter(encounterSpawned); //This should always be the FIRST line of this method
        StartCoroutine(ShowAndHideSpecialScene());
    }

    public override void FinishEncounter()
    {
        StopAllCoroutines();
        base.FinishEncounter(); //This should always be the LAST line of this method.
    }

    private IEnumerator ShowAndHideSpecialScene()
    {
        yield return new WaitForSeconds(0.5f);
        Color color = specialTextScreen.color;
        do
        {
            specialTextScreen.color = Color.Lerp(specialTextScreen.color,new Color(color.r,color.g,color.b,1),Time.deltaTime * 2);
            yield return null;
        } while (specialTextScreen.color.a < 0.9);

        specialTextScreen.color = new Color(color.r, color.g, color.b, 1);
        
        yield return new WaitForSeconds(2f);
        
        do
        {
            specialTextCanvasGroup.alpha = Mathf.Lerp(specialTextCanvasGroup.alpha,0,Time.deltaTime * 2); 
            yield return null;
        } while (specialTextCanvasGroup.alpha > 0.01);
        specialTextCanvasGroup.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        startScreen.SetActive(true);
        GameManager.Instance.gestureController.Pressed += StartGame;
        GameManager.Instance.gestureController.DraggingScreen += StartGame;
    }
    
    public void Win()
    {
        foreach (var fireball in fireballsInGame)
        {
            Destroy(fireball.gameObject);
        }
        foreach (var energyBall in energyballsInGame)
        {
            Destroy(energyBall.gameObject);
        }
        StopAllCoroutines();
        winButton.SetActive(true);
        if (winEvent.IsValid())
        {
            AkSoundEngine.PostEvent(winEvent.Id, gameObject);
        }
    }

    public void Lose()
    {
        foreach (var fireball in fireballsInGame)
        {
            Destroy(fireball.gameObject);
        }
        foreach (var energyBall in energyballsInGame)
        {
            Destroy(energyBall.gameObject);
        }
        
        StopAllCoroutines();
        retryButton.SetActive(true);
        if (loseEvent.IsValid())
        {
            AkSoundEngine.PostEvent(loseEvent.Id, gameObject);
        }
    }

    public void RetryGame()
    {
        startScreen.SetActive(true);
        tutorialFinger.SetActive(true);
        retryButton.SetActive(false);
        GameManager.Instance.gestureController.Pressed += StartGame;
        GameManager.Instance.gestureController.DraggingScreen += StartGame;
    }

    public void StartGame(SwipeInput tapInput)
    {
        GameManager.Instance.gestureController.Pressed -= StartGame;
        GameManager.Instance.gestureController.DraggingScreen -= StartGame;
        retryButton.SetActive(false);
        startScreen.SetActive(false);
        tutorialFinger.SetActive(false);
        core.StartGame();
        StartCoroutine(SpawnFireballs());
        StartCoroutine(SpawnEnergyBalls());
    }

    private IEnumerator SpawnFireballs()
    {
        corePos = core.transform.position;
        do
        {
            yield return new WaitForSeconds(Random.Range(fireballSpawnRate.x,fireballSpawnRate.y));

            float angle = Random.Range(0, 360);
            Vector2 spawnPos =  new Vector2(energySpawnRadius * Mathf.Cos(angle)  + corePos.x,energySpawnRadius * Mathf.Sin(angle)  + corePos.y);

            GameObject fireballInst = Instantiate(fireballPrefab,spawnPos,Quaternion.identity);

            float lookAtAngle = Vector2.SignedAngle(Vector2.down,corePos - spawnPos);
            fireballInst.transform.localEulerAngles = new Vector3(0,0,lookAtAngle);
            
            Fireball fireball = fireballInst.GetComponent<Fireball>();
            fireballsInGame.Add(fireball);
            fireball.encounterLogic = this;
            fireball.Fire(corePos);
        } while (true);
    }
    
    private IEnumerator SpawnEnergyBalls()
    {
        corePos = core.transform.position;
        do
        {
            yield return new WaitForSeconds(Random.Range(energyballSpawnRate.x,energyballSpawnRate.y));

            float angle = Random.Range(0, 360);
            Vector2 spawnPos =  new Vector2(spawnRadius * Mathf.Cos(angle)  + corePos.x,spawnRadius * Mathf.Sin(angle)  + corePos.y);

            GameObject fireballInst = Instantiate(energyPrefab,spawnPos,Quaternion.identity);

            EnergyBall energyBall = fireballInst.GetComponent<EnergyBall>();
            energyballsInGame.Add(energyBall);
            energyBall.encounterLogic = this;
            energyBall.Fire(corePos);
        } while (true);
    }

    public void RemoveFireball(Fireball fireball)
    {
        fireballsInGame.Remove(fireball);
    }
    
    public void RemoveEnergyBall(EnergyBall energyBall)
    {
        energyballsInGame.Remove(energyBall);
    }
    
}
