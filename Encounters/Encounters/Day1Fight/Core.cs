﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Encounters.Encounters.Day1Fight
{
    public class Core : MonoBehaviour
    {
        public Day1FightEncounter encounterLogic;
        
        [SerializeField]
        [BoxGroup("Settings")]
        private int startHealth = 50;
        private int currentHealth = 50;
        [SerializeField]
        [BoxGroup("Settings")]
        private int healthLossOnFirebalHit = 20;
        [SerializeField]
        [BoxGroup("Settings")]
        private int healthGainEnergyHit = 10;
        
        
        [SerializeField]
        [BoxGroup("Sprite")]
        private SpriteRenderer coreSpriteRenderer;
        [BoxGroup("Sprite")]
        public Sprite coreBroken;
        [BoxGroup("Sprite")]
        public Sprite core1;
        [BoxGroup("Sprite")]
        public Sprite core2;
        [BoxGroup("Sprite")]
        public Sprite core3;
        [BoxGroup("Sprite")]
        public Sprite core4;
        [BoxGroup("Sprite")]
        public Sprite core5;
        [BoxGroup("Sprite")]
        public Sprite core6;
        [BoxGroup("Sprite")]
        public Sprite coreFull;

        [BoxGroup("Health")]
        public Image healthGreen;
        [BoxGroup("Health")]
        public Image healthRed;

        [BoxGroup("Audio")]
        public AK.Wwise.Event fireballHitEvent;
        [BoxGroup("Audio")]
        public AK.Wwise.Event energyHitEvent;
        
        private Coroutine healthBarRedCoroutine;

        public void StartGame()
        {
            currentHealth = startHealth;
            SetHealthBar(currentHealth,true);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Fireball>())
            {
                if (fireballHitEvent.IsValid())
                {
                    AkSoundEngine.PostEvent(fireballHitEvent.Id, gameObject);
                }
                
                Destroy(other.gameObject);

                if (currentHealth <= 0) //lose only after being hit once with 0 hp.
                {
                    coreSpriteRenderer.sprite = coreBroken;
                    encounterLogic.Lose();
                }
                
                //If Other has FireballScript
                currentHealth -= healthLossOnFirebalHit;
                SetHealthBar(currentHealth,false);
                
                
                if (currentHealth <= 14)
                {
                    coreSpriteRenderer.sprite = core6;
                } else if (currentHealth <= 28)
                {
                    coreSpriteRenderer.sprite = core5;
                }else if (currentHealth <= 42)
                {
                    coreSpriteRenderer.sprite = core4;
                }else if (currentHealth <= 56)
                {
                    coreSpriteRenderer.sprite = core3;
                }else if (currentHealth <= 70)
                {
                    coreSpriteRenderer.sprite = core2;
                }else if (currentHealth <= 84)
                {
                    coreSpriteRenderer.sprite = core1;
                }else if (currentHealth <= 98)
                {
                    coreSpriteRenderer.sprite = coreFull;
                }
                else
                {
                    coreSpriteRenderer.sprite = coreFull;
                }
            } else if (other.GetComponent<EnergyBall>())
            {
                Destroy(other.gameObject);
                
                if (energyHitEvent.IsValid())
                {
                    AkSoundEngine.PostEvent(energyHitEvent.Id, gameObject);
                }
                
                currentHealth += healthGainEnergyHit;
                SetHealthBar(currentHealth,false);
                
                if (currentHealth <= 14)
                {
                    coreSpriteRenderer.sprite = core6;
                } else if (currentHealth <= 28)
                {
                    coreSpriteRenderer.sprite = core5;
                }else if (currentHealth <= 42)
                {
                    coreSpriteRenderer.sprite = core4;
                }else if (currentHealth <= 56)
                {
                    coreSpriteRenderer.sprite = core3;
                }else if (currentHealth <= 70)
                {
                    coreSpriteRenderer.sprite = core2;
                }else if (currentHealth <= 84)
                {
                    coreSpriteRenderer.sprite = core1;
                }else if (currentHealth <= 98)
                {
                    coreSpriteRenderer.sprite = coreFull;
                }
                else
                {
                    coreSpriteRenderer.sprite = coreFull;
                }

                
                
                if (currentHealth >= 100)
                {
                    encounterLogic.Win();
                }
            }
        }

        private void SetHealthBar(int health, bool setRedImmediately)
        {
            float newFillValue = health / (float)100;
            newFillValue = Mathf.Clamp(newFillValue, 0, 1);

            healthGreen.fillAmount = newFillValue;
            if (setRedImmediately)
            {
                healthRed.fillAmount = newFillValue;
            }
            else
            {
                if (healthBarRedCoroutine != null)
                {
                    StopCoroutine(healthBarRedCoroutine);
                }
                healthBarRedCoroutine = StartCoroutine(SetHealthRed(newFillValue));
            }
        }

        private IEnumerator SetHealthRed(float newFillValue)
        {
            if (newFillValue > healthRed.fillAmount)
            {
                healthRed.fillAmount = newFillValue;
                yield break;
            }
            yield return new WaitForSeconds(1f);
            do
            {
                healthRed.fillAmount = Mathf.Lerp(healthRed.fillAmount, newFillValue, Time.deltaTime * 5);
                yield return null;
            } while (healthRed.fillAmount > newFillValue);
            healthRed.fillAmount = newFillValue;
        }
    }
}
