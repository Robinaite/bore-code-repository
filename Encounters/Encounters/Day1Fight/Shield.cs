﻿using System;
using InputSamples.Gestures;
using TMPro;
using UnityEngine;

namespace Encounters.Encounters.Day1Fight
{
    public class Shield : MonoBehaviour
    {

        private Vector2 corePOs;

        private void Start()
        {
            GameManager.Instance.gestureController.Pressed += PressedScreen;
            GameManager.Instance.gestureController.DraggingScreen += DraggingScreen;
            corePOs = Camera.main.WorldToScreenPoint(transform.position);
        }

        private void DraggingScreen(SwipeInput swipeInput)
        {
           Vector2 vec2 =  swipeInput.PreviousPosition - corePOs;
           float angle = Vector2.SignedAngle(Vector2.up, vec2);
           
           this.transform.localEulerAngles = new Vector3(0,0,angle);
        }

        private void PressedScreen(SwipeInput swipeInput)
        {
            Vector2 vec2 =  swipeInput.StartPosition - corePOs;
            float angle = Vector2.SignedAngle(Vector2.up, vec2);
           
            this.transform.localEulerAngles = new Vector3(0,0,angle);
        }

        private void OnDestroy()
        {
            GameManager.Instance.gestureController.DraggingScreen -= DraggingScreen;
            GameManager.Instance.gestureController.Pressed -= PressedScreen;
        }
    }
}
