﻿using System;
using System.Collections;
using System.Collections.Generic;
using InputSamples.Gestures;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Encounters.Encounters.MiningEncounter
{
    public class MiningEncounter : EncounterSceneLogic
    {
        [BoxGroup("Game Settings")]
        [SerializeField]
        private int bpm = 60;
        [BoxGroup("Game Settings")]
        [SerializeField]
        private int amountOfBeats = 4;
        [BoxGroup("Game Settings")]
        [SerializeField]
        private int swipeDownAngleThreshold = 50;
        [BoxGroup("Game Settings")]
        [SerializeField]
        private float beatSuccessThreshold = 0.1f;

        [BoxGroup("Visuals")]
        [SerializeField]
        private SpriteRenderer ore;
        [SerializeField]
        [BoxGroup("Visuals")]
        private GameObject pickAxe;
        [SerializeField]
        [BoxGroup("Visuals")]
        private float pickaxeRotationThreshold = 30;
        [SerializeField]
        [BoxGroup("Visuals")]
        private float pickaxeBackSpeed = 5f;

        [BoxGroup("UI")] 
        [SerializeField]
        private GameObject startGamePanel;
        [BoxGroup("UI")] 
        [SerializeField]
        private GameObject playersTurnPanel;

        [BoxGroup("UI")] 
        [SerializeField] 
        private GameObject scoreScreen;
        [BoxGroup("UI")] 
        [SerializeField] 
        private TextMeshProUGUI scoreText;
        
  
        [ShowInInspector]
        private List<float> playerHitTimes = new List<float>();
        private bool playersTurn = false;
        
        private float beatsInSeconds = 0;

        private Vector2 screenZero;
        private Vector2 startPos;
        private Coroutine axeRotateBackCoroutine;

        private void Start()
        {
            GameManager.Instance.gestureController.Swiped += Swiped;
            GameManager.Instance.gestureController.Pressed += Pressed;
            GameManager.Instance.gestureController.PotentiallySwiped += Swiping;
        }

        public void StartGame()
        {
            beatsInSeconds = 60 / (float) bpm;
            screenZero = Vector2.zero;
            startGamePanel.SetActive(false);
            playerHitTimes.Clear();
            scoreScreen.SetActive(false);
            StartCoroutine(ShowBeatsToPlayer());
        }

        public override void StartEncounter(SpawnedEncounter encounterSpawned)
        {
            base.StartEncounter(encounterSpawned);
            startGamePanel.SetActive(true);
        }

        public override void FinishEncounter()
        {
            StopAllCoroutines();
            base.FinishEncounter();
        }

        private void Swiped(SwipeInput input)
        {
            if (playersTurn)
            {
                float angle = Vector2.SignedAngle(Vector2.down, input.SwipeDirection);
                if (Mathf.Abs(angle) <= swipeDownAngleThreshold)
                {
                    playerHitTimes.Add(Time.timeSinceLevelLoad);
                    axeRotateBackCoroutine = StartCoroutine(RotatePickaxeBack());
                    StartCoroutine(SetColourOfOre());
                    if (playerHitTimes.Count >= amountOfBeats)
                    {
                        playersTurn = false;
                        ShowScore();
                    }
                
                }
            }
        }

        private void Pressed(SwipeInput input)
        {
            if (playersTurn)
            {
                startPos = input.StartPosition;
                if (axeRotateBackCoroutine != null)
                {
                    StopCoroutine(axeRotateBackCoroutine);
                }
            }
        }

        private void Swiping(SwipeInput input)
        {
            if (playersTurn)
            {
                float angle = Vector2.SignedAngle(Vector2.down, input.SwipeDirection);
                if (Mathf.Abs(angle) <= swipeDownAngleThreshold)
                {
                    Vector2 startPosVec = screenZero-startPos;
                
                    Vector2 currentPosSwipeVec = screenZero -new Vector2(startPos.x,input.PreviousPosition.y) ;
                
                    float newAngle = Mathf.Abs(Vector2.SignedAngle(startPosVec, currentPosSwipeVec));
                    if (newAngle <= pickaxeRotationThreshold)
                    {
                        pickAxe.transform.localEulerAngles = new Vector3(0,0,newAngle);
                    }
                }
            }
        }


        private IEnumerator ShowBeatsToPlayer()
        {
            yield return new WaitForSeconds(1f);
            for (int i = 1; i <= amountOfBeats; i++)
            {
                Debug.Log("Beat: " + i);
                StartCoroutine(SetColourOfOre());
                yield return  new WaitForSeconds(beatsInSeconds);
            }
            
            //Show Player Turn UI
            playersTurnPanel.SetActive(true);
            yield return new WaitForSeconds(3f);
            playersTurnPanel.SetActive(false);
            //hide player UI
            playersTurn = true;
        }

        private IEnumerator SetColourOfOre()
        {
            ore.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            ore.color = Color.white;
        }

        private IEnumerator RotatePickaxeBack()
        {
            do
            {
                yield return null;
                pickAxe.transform.rotation =
                    Quaternion.Lerp(pickAxe.transform.rotation, Quaternion.identity, Time.deltaTime * pickaxeBackSpeed);
            } while (pickAxe.transform.rotation != Quaternion.identity);
        }

        private void ShowScore()
        {

            int succesfulBeat = 0;
            //Calculate Score.
            float playerStart = playerHitTimes[0];

            float[] timeBetweenHits = new float[amountOfBeats - 1];
            
            for (int i = 1; i < playerHitTimes.Count; i++)
            {
                timeBetweenHits[i - 1] = playerHitTimes[i] - (playerHitTimes[0]+(beatsInSeconds*i));
                
            }

            foreach (var timeBetweenHit in timeBetweenHits)
            {
                Debug.Log(timeBetweenHit);
                if (Mathf.Abs(timeBetweenHit) <= beatSuccessThreshold)
                {
                    succesfulBeat++;
                }
            }
            
            Debug.Log(succesfulBeat);
            scoreText.text = (succesfulBeat + 1) + "/" + amountOfBeats;
            scoreScreen.SetActive(true);

        }

        private void OnDestroy()
        {
            GameManager.Instance.gestureController.Swiped -= Swiped;
            GameManager.Instance.gestureController.Pressed -= Pressed;
            GameManager.Instance.gestureController.PotentiallySwiped -= Swiping;
        }
    }
}
