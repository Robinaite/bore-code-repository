﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "boolRequirement",menuName = "Encounters/Requirements/Boolean",order = 0)]
public class BoolReq : BaseRequirement
{
    [SerializeField] private bool boolReq = false;
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        return boolReq;
    }
}
