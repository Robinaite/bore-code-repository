﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Amount of Caverns",menuName = "Encounters/Requirements/Amount of Caverns",order = 0)]
public class AmountOfCavernsReq : BaseRequirement
{
    [InfoBox("Encounter only happens on specific day.")]
    [SerializeField]
    private int amountOfCavern = 0;

    [SerializeField]
    private bool orHigher = false;
    [SerializeField]
    private bool orLower = false;
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        if (orHigher)
        {
            return amountOfCavern <= GameManager.Instance.ObtainTotalCavernAmount();
        }

        if (orLower)
        {
            return amountOfCavern >= GameManager.Instance.ObtainTotalCavernAmount();
        }
        return amountOfCavern == GameManager.Instance.ObtainTotalCavernAmount();
    }
}