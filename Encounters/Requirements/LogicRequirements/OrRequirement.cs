﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Or",menuName = "Encounters/Requirements/Logic/Or",order = 0)]
public class OrRequirement : BaseRequirement
{
    [SerializeField]
    [InfoBox("One the requirements need to be valid to return true")]
    private List<BaseRequirement> requirements = new List<BaseRequirement>();
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        bool validated = false;

        foreach (var baseRequirement in requirements)
        {
            validated = validated || baseRequirement.ValidateRequirement(cavern);
        }

        return validated;
    }
}
