﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "And",menuName = "Encounters/Requirements/Logic/And",order = 0)]
public class AndRequirement : BaseRequirement
{
    [SerializeField]
    [InfoBox("All the requirements need to be valid to return true")]
    private List<BaseRequirement> requirements = new List<BaseRequirement>();
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        bool validated = true;

        foreach (var baseRequirement in requirements)
        {
            validated = validated && baseRequirement.ValidateRequirement(cavern);
        }

        return validated;
    }
}
