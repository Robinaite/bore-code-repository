﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "DayCountAndStateReq",menuName = "Encounters/Requirements/Day Count and State",order = 0)]
public class DayCountAndStateReq : BaseRequirement
{
    [InfoBox("Encounter only happens on specific day.")]
    [SerializeField]
    private bool enableDayCount = false;
    
    [InfoBox("Encounter only happens on specific day.")]
    [ShowIf("enableDayCount")]
    [SerializeField] private int dayCount = 0;
    [SerializeField]
    private bool enableDayState = false;
    [SerializeField]
    [InfoBox("At which state of the day should the encounter manifest!")]
    [ShowIf("enableDayState")]
    private Day.DayState dayState = Day.DayState.StartDay;
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        bool result = true;
        if (enableDayCount)
        {
            result &= GameManager.Instance.dayManager.GetCurrentDayNumber() == dayCount;
        }

        if (enableDayState)
        {
            result &= GameManager.Instance.dayManager.IsCurrentDayState(dayState);
        }
        
        return (enableDayCount || enableDayState) && result;
    }
}