﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "ResourceType", menuName = "Encounters/Requirements/Resource Type", order = 0)]
public class CavernTypeReq : BaseRequirement
{
    [InfoBox("Encounter only happens if cavern is of specific type of resource")]
    [SerializeField]
    private ResourceType.Types type = ResourceType.Types.NONE;

    public override bool ValidateRequirement(Cavern cavern)
    {
        return cavern.GetResourceType() == type;
    }
}