﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "levelReq",menuName = "Encounters/Requirements/Cavern Level",order = 0)]
public class CavernLevelReq : BaseRequirement
{
    [InfoBox("Validates if the requirement of the cavern it is happening is equal")]
    [SerializeField] private int levelRequirement = 0;

    [SerializeField]
    private bool orHigher = false;
    [SerializeField]
    private bool orLower = false;
    
    public override bool ValidateRequirement(Cavern cavern)
    {
        if (cavern == null)
        {
            Debug.LogWarning("There is a requirement being called on an event encounter not related to a cavern. PLease recheck the event encounters on the DayManager");
            return false;
        }
        
        if (orHigher)
        {
            return levelRequirement <= cavern.GetLevel();
        }

        if (orLower)
        {
            return levelRequirement >= cavern.GetLevel();
        }
        return levelRequirement == cavern.GetLevel();
    }
}
