﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class BaseRequirement : SerializedScriptableObject
{
    /// <summary>
    /// Executes all the validations needed to allow the spawning of the encounter.
    /// </summary>
    /// <param name="cavern">THIS CAN BE NULL!</param>
    /// <returns></returns>
    public abstract bool ValidateRequirement(Cavern cavern);
}
