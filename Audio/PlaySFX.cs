﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{

    public AudioClip audioCLip;

    public void PlaySound()
    {
        if (audioCLip)
        {
            GameManager.Instance.audioManager.PlaySFX(audioCLip);
        }
    }
}
