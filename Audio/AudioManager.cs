﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AK.Wwise.Event musicEvent;

    public enum MusicMapState
    {
        FoodCavern,
        FoodEncounter,
        Hub,
        MainMenu ,
        OreCavern,
        SpecialEncounterDay1,
        Wireframe,
        WoodCavern,
        WoodEncounter,
        ChapterOneToTwo
    }

    private void Start()
    {
        AkSoundEngine.SetState("MapState", MusicMapState.MainMenu.ToString());
        AkSoundEngine.PostEvent(musicEvent.Id,gameObject);
    }


    public void PlaySFX(AudioClip clipToPlay)
    {
        //sfxSource.PlayOneShot(clipToPlay);
    }

    
}
