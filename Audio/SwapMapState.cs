﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMapState : MonoBehaviour
{
    public AudioManager.MusicMapState mapState = AudioManager.MusicMapState.MainMenu;

    private void Start()
    {
        AkSoundEngine.SetState("MapState", mapState.ToString());
    }
}
