﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class PeopleInCavern : MonoBehaviour
{
    private People peopleRef; //This is null in Cavern Interiors for now
    [SerializeField]
    private Animator peopleAnimator;

    [SerializeField] 
    private SpriteRenderer peopleSpriteRenderer;
    [SerializeField]
    private Vector2 timeBeforeNewDirection = new Vector2(1f,5f);
    [SerializeField]
    private float movementSpeed = 2f;
    [SerializeField]
    [MinValue(0),MaxValue(100)]
    private int chanceOfNotMoving = 30;
    
    public Rigidbody2D rigidBody;

    private Vector2 force;
    private float originalDragValue;
    private int baseSortingOrder = 0;
    
    
    public void Setup(People people, int sortingOrder = 0)
    {
        peopleRef = people;
        peopleAnimator.SetFloat("Offset",Random.Range(0f,1f));
        peopleSpriteRenderer.sortingOrder = sortingOrder + 5;
        baseSortingOrder = sortingOrder + 5;
        originalDragValue = rigidBody.drag;
        StartCoroutine(Movement());
    }

    private void Update()
    {
        peopleAnimator.SetFloat("MovementSpeed",rigidBody.velocity.magnitude);
        peopleAnimator.SetBool("Mirror",rigidBody.velocity.x < 0);
        peopleSpriteRenderer.transform.localPosition = new Vector3(0,0,5 + transform.localPosition.y);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void WalkInNewDirection()//TODO, potentially add some centre bias, so that there's more chance they walk through the center of the area.
    {
        force = new Vector2(Random.Range(-1f,1f),Random.Range(-1f,1f)) * movementSpeed;
    }

    IEnumerator Movement() 
    {
        do
        {
            int notMoving = Random.Range(0, 100);
            if (notMoving < chanceOfNotMoving)
            {
                rigidBody.drag = 1000;
            }
            else
            {
                rigidBody.drag = originalDragValue;
                WalkInNewDirection();
                rigidBody.AddForce(force);
            }
            
            yield return new WaitForSeconds(Random.Range(timeBeforeNewDirection.x,timeBeforeNewDirection.y));
        } while (true);
    }
    
}
