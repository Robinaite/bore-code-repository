﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class People
{
    //Number of days this person hasn't had food
    int daysSkipped = 0;

    HubHandler hub;

    public People()
    {
        hub = GameManager.Instance.hubHandler;
    }

    public void consumeFood()
    {
        if (hub.ObtainResourceAmount(ResourceType.Types.FOOD) > 0)
        {
            hub.takeResource(1,ResourceType.Types.FOOD);
            daysSkipped = 0;
        }
        else
        {
            skipFoodDay();
        }
    }

    public void skipFoodDay()
    {
        daysSkipped += 1;

        if (daysSkipped >= 3)
        {
            hub.removePersonFromHub(this);
        }
    }

}
