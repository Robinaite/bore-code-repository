﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DayManager : MonoBehaviour
{
    private Day currentDay;
    
    [SerializeField]
    private List<EncounterData> dayEncounters = new List<EncounterData>();
    private List<EncounterData> usableDayEncounters;

    [SerializeField]
    private List<EncounterData> cavernDayEncounters = new List<EncounterData>();
    
    HubHandler hub;

    public delegate void NewDayDelegate();
    public NewDayDelegate newDayDel;

    private int lastGameDay = 0;
    
    private void Awake()
    {
        hub = GameManager.Instance.hubHandler;
    }
    
    
    public void StartGame()
    {
        lastGameDay = 0;
        usableDayEncounters = new List<EncounterData>(dayEncounters);
        currentDay = new Day(0,hub.getTotalHub());
        currentDay.StartDay();
        
    }
    

    /// <summary>
    /// This method adds a people from the daily pool to the cavern indicated.
    /// </summary>
    /// <param name="cavern"></param>
    /// <returns>TRUE - if able to add people to caver, FALSE - if pool is empty</returns>
    public bool AddPeopleToCavern(Cavern cavern)
    {
        return currentDay.AddPeopleToCavern(cavern);
    }

    /// <summary>
    /// This method removes a person from the cavern and adds it back into the daily pool
    /// </summary>
    /// <param name="cavern"></param>
    /// <returns>TRUE - if able to remove people from the cavern, FALSE - if pool is empty</returns>
    public bool RemovePeopleFromCavern(Cavern cavern)
    {
        return currentDay.RemovePeopleFromCavern(cavern);
    }

    public int GetAmountOfPeopleFromCavern(Cavern cavern)
    {
        return currentDay.GetAmountOfPeopleFromCavern(cavern);
    }

    public int GetCurrentPeoplePoolSize()
    {
        if (ReferenceEquals(currentDay, null))
        {
            return 0;
        }

        return currentDay.GetDailyPoolSize();
    }

    public int GetCurrentDayNumber()
    {
        if (ReferenceEquals(currentDay, null))
        {
            return 0;
        }
        return currentDay.GetDayNumber();
    }
    

    public void NextDayState()
    {
       Day.DayState dayState = currentDay.NextDayState();

       if (dayState == Day.DayState.Finished)
       { 
           //NEXT DAY!
           GameManager.Instance.globalCanvasUI.ShowNextDayBanner(NewDay);
       }
    }

    private void NewDay()
    {
        currentDay.FinaliseDay();
        Day newDay = new Day(currentDay.GetDayNumber(), hub.getTotalHub());
        currentDay = newDay;
        newDayDel?.Invoke();
        currentDay.StartDay();
    }

    public int GetPotentialResource(ResourceType.Types resourceType)
    {
        if (ReferenceEquals(currentDay, null))
        {
            return 0;
        }
        
        return currentDay.GetPotentialResources(resourceType);
    }

    public List<SpawnedEncounter> GetSpawnedEncountersOfCavern(Cavern cavernData)
    {
        if (ReferenceEquals(currentDay, null))
        {
            return new List<SpawnedEncounter>();
        }
        return currentDay.GetSpawnedEncountersFromCavern(cavernData);
    }

    public bool IsCurrentDayState(Day.DayState dayState)
    {
        return currentDay.GetCurrentDayState() == dayState;
    }

    public Day.DayState GetCurrentDayState()
    {
        return currentDay.GetCurrentDayState();
    }

    public void SpawnDayEncounters()
    {
        foreach (var encounter in usableDayEncounters)
        {
            if (encounter.ValidateEncounter(null))
            {
                if (encounter.goToMainMenu)
                {
                    if (lastGameDay > 0 && lastGameDay < currentDay.GetDayNumber())
                    {
                        currentDay.SpawnDayEncounter(encounter);
                        usableDayEncounters.Remove(encounter);
                        break;
                    }
                    
                    lastGameDay = currentDay.GetDayNumber();
                    continue;
                }
                
                currentDay.SpawnDayEncounter(encounter);
                usableDayEncounters.Remove(encounter);
                break;
            }
        }
    }

    public List<EncounterData> ObtainCavernEncounters()
    {
        return cavernDayEncounters;
    }

    public bool ArePeopleSendToCaves()
    {
        return GetCurrentPeoplePoolSize() < hub.GetPeopleTotalAmount();
    }

    public void FoundNewPeople(int amount)
    {
        currentDay.FoundNewPeople(amount);
    }

    public int GetPotentialNewPeople()
    {
        return currentDay.GetPotentialNewPeople();
    }

    public void SetCavernMultiplier(Cavern cavern, float multiplier)
    {
        currentDay.SetCavernMultiplier(cavern,multiplier);
    }
   
}
