﻿using System.Collections.Generic;
using UnityEngine;

public class DayCavern
{
    private Cavern cavern;
    private Stack<People> peoples; //Check the class
    private List<SpawnedEncounter> spawnedEncounters;

    private float encounterMultiplier = 1f;

    public DayCavern(Cavern cavern)
    {
        this.cavern = cavern;
        peoples = new Stack<People>();
        spawnedEncounters = new List<SpawnedEncounter>();
    }

    public bool IsOfCavern(Cavern cavern)
    {
        return this.cavern == cavern;
    }

    public void ReceivePeople(People people)
    {
        peoples.Push(people);
    }

    public People RetrievePeople()
    {
        return peoples.Pop();
    }

    public int GetAmountOfPeople()
    {
        return peoples.Count;
    }

    public int GetResources()
    {
        return Mathf.RoundToInt(cavern.ObtainResources(peoples.Count) * encounterMultiplier);
    }

    public void SetEncounterMultiplier(float multiplier)
    {
        encounterMultiplier = multiplier;
    }

    public void SetupCave()
    {
        if (GetResourceTypeOfCavern() != ResourceType.Types.NONE && cavern.GetLevel() == 0 && peoples.Count >= cavern.ObtainAmountOfPeopleRequiredToOpenCave())
        {
            cavern.UpgradeLevel();
        }
    }

    public ResourceType.Types GetResourceTypeOfCavern()
    {
        return cavern.GetResourceType();
    }

    public void SpawnEncounters()
    {
        if (peoples.Count > 0)
        {
            foreach (var possibleEncounter in GameManager.Instance.dayManager.ObtainCavernEncounters())
            {
                if (possibleEncounter.ValidateEncounter(cavern))
                {
                    SpawnedEncounter newEncounter = new SpawnedEncounter(possibleEncounter);
                    spawnedEncounters.Add(newEncounter);
                    cavern.ToggleCavernWireframeEncounterAlert(true);
                    break; //For now only spawn at max 1 encounter per cavern per day.
                }
            }
        }
    }

    public bool AreEncountersDone()
    {
        bool done = true;
        foreach (var spawnedEncounter in spawnedEncounters)
        {
            done = done && spawnedEncounter.IsEncounterDone();
        }

        return done;
    }

    public List<SpawnedEncounter> GetSpawnedEncounters()
    {
        return spawnedEncounters;
    }
    
}