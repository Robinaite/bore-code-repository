﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hub : MonoBehaviour
{
    //Hub handler script reference
    HubHandler hub;

    //Prefab used for spawning in new people
    [SerializeField]
    private PeopleInCavern peoplePrefab;
    
    
    List<PeopleInCavern> peopleSpawned = new List<PeopleInCavern>();

    private void OnEnable()
    {
        hub = GameManager.Instance.hubHandler;
        SpawnHubPeople();
        hub.setInUpgradeMenu(false);
    }

    private void OnDisable()
    {
        DestroyHubPeople();
    }

    public void BackToMap()
    {
        GameManager.Instance.sceneLoader.LoadScene("WireframeMap");
    }

    //Set up which resource type the player would like to upgrade
    public void upgradeType(ResourceType r)
    {
        hub.setResource(r);
        hub.setInUpgradeMenu(true);
    }

    private void SpawnHubPeople()
    {
        PeopleSpawnLayer spawnLayer = FindObjectOfType<PeopleSpawnLayer>();
        int poolSize = GameManager.Instance.dayManager.GetCurrentPeoplePoolSize();
        for(int i = 0; i< poolSize;i++)
        {
           PeopleInCavern peopleInCavern = Instantiate(peoplePrefab, spawnLayer.GetSpawnPoint(), Quaternion.identity,spawnLayer.transform);
           peopleInCavern.Setup(null,spawnLayer.spriteRenderer.sortingOrder); 
           peopleSpawned.Add(peopleInCavern);
        }
    }

    private void DestroyHubPeople()
    {
        for (int i = peopleSpawned.Count - 1; i == 0; i--)
        {
            PeopleInCavern peopleInCavern = peopleSpawned[i];
            peopleSpawned.RemoveAt(i);
            Destroy(peopleInCavern.gameObject);
        }
    }
    
}
