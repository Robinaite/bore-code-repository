﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day
{
    public  enum DayState
    {
        StartDay = 0,
        Planning = 1,
        TransitionToCaverns = 2,
        Encounters = 3,
        TransitionToNextDay = 4,
        Finished = 5,
    }

    private DayState dayState;
    private int dayNumber;
    private List<DayCavern> cavernsOfThisDay;
    private Stack<People> dailyPeoplePool;
    private SpawnedEncounter spawnedDayEncounter;
    private int newPeople = 0;
    
    //Resources from encounters
    int r_FoodDay = 0;
    int r_OreDay = 0;
    int r_WoodDay = 0;

    public Day(int previousDayNumber, List<People> totalPeopleOnNewDay)
    {
        dayNumber = previousDayNumber + 1;
        dayState = DayState.StartDay;
        cavernsOfThisDay = new List<DayCavern>();
        dailyPeoplePool = new Stack<People>(totalPeopleOnNewDay);
        r_FoodDay = 0;
        r_OreDay = 0;
        r_WoodDay = 0;
    }

    public DayState GetCurrentDayState()
    {
        return dayState;
    }

    public int GetDayNumber()
    {
        return dayNumber;
    }

    public int GetDailyPoolSize()
    {
        return dailyPeoplePool.Count;
    }
    
    /// <summary>
    /// Executes the necessary methods of the next day
    /// </summary>
    /// <returns>return the new day state when finished executing</returns>
    public DayState NextDayState()
    {
        switch (dayState)
        {
            case DayState.StartDay:
                StartPlanningState();
                break;
            case DayState.Planning:
                StartTransitionToCavernsState();
                break;
            case DayState.TransitionToCaverns:
                StartEncountersState();
                break;
            case DayState.Encounters:
                StartTransitionToNextDayState();
                break;
            case DayState.TransitionToNextDay:
                StartNextDay();
                break;
            case DayState.Finished:
                Debug.Log("This day has already been finished and should not be called again. Day NUmber: " + dayNumber);
                return DayState.Finished;
            default:
                return DayState.Finished;
        }

        return dayState;
    }

    public void StartDay()
    {
        

        
        //Executes the food resources managment of the people.
        if (!CheckForDayEncounters())
        {
            return;
        }

        NextDayState();
    }



    private void StartPlanningState()
    {
        if (!CheckForDayEncounters())
        {
            return;
        }
        
        
        dayState = DayState.Planning;
        //Do all the necessary allocations for when starting the planning state
        Debug.Log("Start Planning State");
        

        //Not sure if this is needed, but make sure people within the pool are updated
        dailyPeoplePool = new Stack<People>(GameManager.Instance.hubHandler.getTotalHub());

        CheckForDayEncounters();

    }

    private void StartTransitionToCavernsState()
    {
        if (!CheckForDayEncounters())
        {
            return;
        }
        
        dayState = DayState.TransitionToCaverns;
        //Do all the necessary allocations for when transitioning to the cavern. e.g animations, and prepare calculations for encounters
        Debug.Log("Transitioning to caverns");
        CheckForDayEncounters();
        NextDayState();
    }

    private void StartEncountersState()
    {
        if (!CheckForDayEncounters())
        {
            return;
        }
        dayState = DayState.Encounters;
        Debug.Log("Transitioning to encounters");
        
        //Do all the necessary stuff to show the encounters that should appear on the map.
        foreach (var dayCavern in cavernsOfThisDay)
        {
            dayCavern.SpawnEncounters();
        }
        CheckForDayEncounters();
        NextDayState();
    }

    private void StartTransitionToNextDayState()
    {
        if (!CheckForDayEncounters())
        {
            return;
        }
        //Only transition if all encounters are done.
        if (!AreAllEncountersDone())
        {
            Debug.Log("Not all encounters are done!");
            return;
        }

        dayState = DayState.TransitionToNextDay;
        //animate the people going back to the hub. 
        CheckForDayEncounters();
        NextDayState();
    }

    private void StartNextDay()
    {
        if (!CheckForDayEncounters())
        {
            return;
        }
        dayState = DayState.Finished;
        //save all the resources obtained in the hub or upgrade Caverns that got people sent to for the first time
    }

    public void FinaliseDay()
    {
        HubHandler hubHandler = GameManager.Instance.hubHandler;
        hubHandler.PeopleConsumeFood();
        
        
        foreach (var cavernResource in cavernsOfThisDay)
        {
            switch (cavernResource.GetResourceTypeOfCavern())
            {
                case ResourceType.Types.WOOD:
                    r_WoodDay += cavernResource.GetResources();
                    break;
                case ResourceType.Types.ORE:
                    r_OreDay += cavernResource.GetResources();
                    break;
                case ResourceType.Types.FOOD:
                    r_FoodDay += cavernResource.GetResources();
                    break;
            }

            cavernResource.SetupCave();
        }

        
        hubHandler.addResource(r_WoodDay, ResourceType.Types.WOOD);
        hubHandler.addResource(r_FoodDay, ResourceType.Types.FOOD);
        hubHandler.addResource(r_OreDay, ResourceType.Types.ORE);
        
        for(int i =0; i < newPeople; i++)
        {
            People person = new People();
            hubHandler.addNewPeople(person);
        }
    }
    
    public void FoundNewPeople(int amount)
    {
        newPeople += amount;
    }

    public int GetPotentialNewPeople()
    {
        return newPeople;
    }

    /// <summary>
    /// Spawns an encounter if the requirements are met for that day.
    /// And checks if the encounter is done. Only one encounter per day is possible at any day state.
    /// </summary>
    /// <returns></returns>
    private bool CheckForDayEncounters()
    {
        GameManager.Instance.dayManager.SpawnDayEncounters();
        return IsEncounterDone();
    }
    
    private bool AreAllEncountersDone()
    {
        bool encountersDone = true;
        foreach (var dayCavern in cavernsOfThisDay)
        {
            encountersDone = encountersDone && dayCavern.AreEncountersDone();
        }

        return encountersDone;
    }

    public List<SpawnedEncounter> GetSpawnedEncountersFromCavern(Cavern cavernData)
    {
        if (dayState != DayState.Encounters)
        {
            return new List<SpawnedEncounter>();
        }
        
        DayCavern dayCavern = cavernsOfThisDay.Find(cavern => cavern.IsOfCavern(cavernData));
        if (dayCavern != default(DayCavern))
        {
            return dayCavern.GetSpawnedEncounters();
        }

        return new List<SpawnedEncounter>();
    }

    public bool AddPeopleToCavern(Cavern cavern)
    {
        //1. Check if a DayCavernExists of this cavern.
        DayCavern dayCavern = cavernsOfThisDay.Find(dayCavernToFind => dayCavernToFind.IsOfCavern(cavern));
        if (dayCavern == default(DayCavern))
        {
            dayCavern = new DayCavern(cavern);
            cavernsOfThisDay.Add(dayCavern);
        }

        if (dailyPeoplePool.Count > 0)
        {
            dayCavern.ReceivePeople(dailyPeoplePool.Pop());
            return true;
        }
        return false;
    }
    
    public bool RemovePeopleFromCavern(Cavern cavern)
    {
        //1. Check if a DayCavernExists of this cavern.
        DayCavern dayCavern = cavernsOfThisDay.Find(dayCavernToFind => dayCavernToFind.IsOfCavern(cavern));
        if (dayCavern == default(DayCavern))
        {
            return false;
        }

        try
        {
            People people = dayCavern.RetrievePeople();
            dailyPeoplePool.Push(people);
            return true;
        }
        catch (InvalidOperationException)
        {
            return false;
        }
    }

    public int GetAmountOfPeopleFromCavern(Cavern cavern)
    {
        DayCavern dayCavern = cavernsOfThisDay.Find(dayCavernToFind => dayCavernToFind.IsOfCavern(cavern));
        if (dayCavern == default(DayCavern))
        {
            return 0;
        }

        return dayCavern.GetAmountOfPeople();
    }

    public int GetPotentialResources(ResourceType.Types resourceType)
    {
        int potentialResourcesObtained = 0;

        foreach (var cavernResource in cavernsOfThisDay)
        {
            if (cavernResource.GetResourceTypeOfCavern() == resourceType)
            {
                potentialResourcesObtained += cavernResource.GetResources();
            }
        }
        switch (resourceType)
        {
            case ResourceType.Types.WOOD:
                potentialResourcesObtained += r_WoodDay;
                break;
            case ResourceType.Types.ORE:
                potentialResourcesObtained += r_OreDay;
                break;
            case ResourceType.Types.FOOD:
                potentialResourcesObtained += r_FoodDay;
                break;
        }
        
        return potentialResourcesObtained;
    }

    public void SpawnDayEncounter(EncounterData encounterData)
    {
        spawnedDayEncounter = new SpawnedEncounter(encounterData);
        spawnedDayEncounter.StartEncounter(null,null);
    }

    public bool IsEncounterDone()
    {
        if (spawnedDayEncounter == null)
        {
            return true;
        }

        return spawnedDayEncounter.IsEncounterDone();
    }

    public void SetCavernMultiplier(Cavern cavern, float multiplier)
    {
        DayCavern dayCavern = cavernsOfThisDay.Find(dayCavernToFind => dayCavernToFind.IsOfCavern(cavern));
        dayCavern?.SetEncounterMultiplier(multiplier);
    }
    
}
