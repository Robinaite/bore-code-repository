﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubWireframe : MonoBehaviour
{
    [SerializeField]
    private GameObject peopleToSpawn;
    [SerializeField]
    private List<Transform> peopleSpawnPoints = new List<Transform>();
    private int lastSpawnPointUsed = 0;
    private Stack<GameObject> peopleSpawned = new Stack<GameObject>();

    [SerializeField]
    EncounterData shaftTutorial;
    SpawnedEncounter encounter;

    private void Start()
    {
        if(!GameManager.Instance.getSpawnedTutorial())
        {
            encounter = new SpawnedEncounter(shaftTutorial);
            encounter.StartEncounter(null, setTutorialCompleted, true);
        }
    }
    public void OnHubClick()
    {
        GameManager.Instance.sceneLoader.LoadScene(GameManager.Instance.hubHandler.GetHubScenePath());
    }

    private void OnEnable()
    {
        GameManager.Instance.dayManager.newDayDel += SpawnPeople;
        SpawnPeople();
    }
    
    private void OnDisable()
    {
        if (GameManager.Instance)
        {
            GameManager.Instance.dayManager.newDayDel -= SpawnPeople;
        }
    }

    public void SpawnPeople()
    {
        int peopleSpawnedCount = peopleSpawned.Count;
        for (int i = 0; i < peopleSpawnedCount; i++)
        {
            GameObject peopleInCavern = peopleSpawned.Pop();
            Destroy(peopleInCavern);
        }
        
        for (int i = 0; i < GameManager.Instance.dayManager.GetCurrentPeoplePoolSize();i++)
        {
            peopleSpawned.Push(Instantiate(peopleToSpawn, peopleSpawnPoints[lastSpawnPointUsed]));
            lastSpawnPointUsed++;
            if (lastSpawnPointUsed >= peopleSpawnPoints.Count)
            {
                lastSpawnPointUsed = 0;
            }
        }
    }

    public void setTutorialCompleted()
    {
        Debug.Log("CALLED TUTORIAL COMPLETED");
        GameManager.Instance.setSpawnedTutorial(true);
    }
}
