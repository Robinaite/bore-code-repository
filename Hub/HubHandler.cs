﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HubHandler : MonoBehaviour
{
    [SerializeField]
    List<People> availableHubPeople = new List<People>();

    [SerializeField]
    private int startFoodAmount = 20;
    [SerializeField]
    private int startWoodAmount = 5;
    [SerializeField]
    private int startOreAmount = 5;
    
    //Resources
    
    int r_FoodTotal = 20;
    int r_OreTotal = 5;
    int r_WoodTotal = 5;

    //Standard int to assign the starting amount of people the player gets
    int startingPeopleCount = 5;


    ResourceType resource;

    bool inUpgradeMenu = false;

    [SerializeField]
    private string hubScene = "";

    public string GetHubScenePath()
    {
        return hubScene;
    }

    public void StartGame()
    {
        availableHubPeople.Clear();
        
        for (int i = 0; i < startingPeopleCount; i++)
        {
            People p = new People();
            addNewPeople(p);
        }

        r_FoodTotal = startFoodAmount;
        r_OreTotal = startOreAmount;
        r_WoodTotal = startWoodAmount;

        //GameManager.Instance.sceneLoader.LoadScene(hubScene);
    }


    //Add new resources gained to the overall total for the cavern system/hub
    public void addResource(int amount, ResourceType.Types type)
    {
        //Assign the added resources to the correct total
        switch (type)
        {
            case ResourceType.Types.WOOD:
                r_WoodTotal += amount;
                break;
            case ResourceType.Types.ORE:
                r_OreTotal += amount;
                break;
            case ResourceType.Types.FOOD:
                r_FoodTotal += amount;
                break;
            default: Debug.Log("Invalid resource type.");
                break;
        }

    }
    public void takeResource(int amount, ResourceType.Types type)
    {
        //Assign the added resources to the correct total
        switch (type)
        {
            case ResourceType.Types.WOOD:
                r_WoodTotal -= amount;
                break;
            case ResourceType.Types.ORE:
                r_OreTotal -= amount;
                break;
            case ResourceType.Types.FOOD:
                r_FoodTotal -= amount;
                break;
            default:
                Debug.Log("Invalid resource type.");
                break;
        }

    }

    public bool checkAvaliableResource(int amount, ResourceType.Types type)
    {
        //Store the correct resource type in a variable
        int resourceTotal = 0;

        switch (type)
        {
            case ResourceType.Types.WOOD:
                resourceTotal = r_WoodTotal;
                break;
            case ResourceType.Types.ORE:
                resourceTotal = r_OreTotal;
                break;
            case ResourceType.Types.FOOD:
                resourceTotal = r_FoodTotal;
                break;
            default:
                Debug.Log("Invalid resource type.");
                break;
        }

        //If there are the needed amount of resources avaliable return true
        if(resourceTotal - amount >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    public int ObtainResourceAmount(ResourceType.Types type)
    {
        switch (type)
        {
            case ResourceType.Types.WOOD:
                return r_WoodTotal;
            case ResourceType.Types.ORE:
                return r_OreTotal;
            case ResourceType.Types.FOOD:
                return r_FoodTotal;
            default:
                Debug.Log("Invalid resource type.");
                return 0;
        }
    }
    
    public int GetPeopleTotalAmount()
    {
        return availableHubPeople.Count;
    }

    public void addNewPeople(People p)
    {
        availableHubPeople.Add(p);
    }
    
    public void gatherNewPeople(int a)
    {
        
    }
    public void removePersonFromHub(People p)
    {
        availableHubPeople.Remove(p);
        if (availableHubPeople.Count <= 0)
        {
            Debug.Log("Game Over. Restart!");
            GameManager.Instance.globalCanvasUI.ShowMainMenu();
        }
    }

    public List<People> getTotalHub()
    {
        return availableHubPeople;
    }

    public void setResource(ResourceType r)
    {
        resource = r;
    }

    //Pass in the cavern selected
    public void upgradeCavern(CavernWireframe cavern)
    {
        //Cavern c = GameManager.Instance.ObtainCavernFromData(cavern);

        //If the cavern is the same as the resource selected, upgrade the resource production
        if (GameManager.Instance.ObtainCavernFromData(cavern).GetResourceType() == resource.getType())
        {
            //if ((resource.getAmount() - c.getUpgradeCost()) >= 0)
            //{
            GameManager.Instance.ObtainCavernFromData(cavern).UpgradeLevel();
                resource.changeAmount(-GameManager.Instance.ObtainCavernFromData(cavern).getUpgradeCost());
            
            //}
            //else
            //{
            //    Debug.Log("INSUFFICIENT FUNDS!");
            //}
        }
        else
        {
            Debug.Log("CAVERN UPGRADE DOES NOT MATCH!");
        }

        setInUpgradeMenu(false);
        resource = null;
    }

  
    public void setInUpgradeMenu(bool set)
    {
        inUpgradeMenu = set;
    }

    public bool getInUpgradeMenu()
    {
        return inUpgradeMenu;
    }

    public void PeopleConsumeFood()
    {
        foreach (var people in availableHubPeople)
        {
            people.consumeFood();
        }
    }
}
