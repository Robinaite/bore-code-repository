﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class HideGameObjectIfOtherDayState : MonoBehaviour
{
    [InfoBox("State where this game object is shown.")]
    public Day.DayState activeState;
    
    private void OnEnable()
    {
        gameObject.SetActive(GameManager.Instance.dayManager.IsCurrentDayState(activeState));
    }
}
