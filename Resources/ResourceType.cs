﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "ResourceName",menuName = "Resource Type",order = 0)]
public class ResourceType : SerializedScriptableObject
{
    //Enum for types of resources
    public enum Types { WOOD, ORE, FOOD, NONE};
    [SerializeField]
    private Sprite icon;
    [SerializeField]
    [ListDrawerSettings(DraggableItems = true)]
    private List<int> amountPerColonistPerUpgrade = new List<int>();
    [SerializeField]
    private Types type;
    [SerializeField]
    private Dictionary<Types,int> installationCost = new Dictionary<Types, int>();

    public Types getType()
    {
        return type;
    }

    public int GetAmountPerColonist(int cavernUpgrade)
    {
        if (cavernUpgrade-1 > amountPerColonistPerUpgrade.Count)
        {
            if (amountPerColonistPerUpgrade.Count == 0)
            {
                return 0;
            }
            
            return amountPerColonistPerUpgrade[amountPerColonistPerUpgrade.Count - 1];
        }
        
        
        return amountPerColonistPerUpgrade[cavernUpgrade-1];
    }

    private Dictionary<Types, int> GetInstallationCost()
    {
        return installationCost;
    }
    
    public void changeAmount(int a)
    {
        
    }

    public int getAmount()
    {
        return 0;
    }
}
