﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateGameManager : MonoBehaviour
{

    public GameObject gameManager;
    
    // Start is called before the first frame update
    private void Awake()
    {
        if (!GameObject.FindWithTag("GameManager"))
        {
            Instantiate(gameManager);
        }
    }
}

