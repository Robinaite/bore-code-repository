﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowIfState : MonoBehaviour
{
    [SerializeField]
    private List<ResourceType.Types> types = new List<ResourceType.Types>();
    
    [SerializeField]
    private List<Day.DayState> dayStates = new List<Day.DayState>();

    [SerializeField]
    private CavernInterior cavernInterior;
    
    private void OnEnable()
    {
        if (!cavernInterior)
        {
            cavernInterior = FindObjectOfType<CavernInterior>();
            Debug.LogWarning("This should be not getting in here, as find object is expensive!", this);
        }

        if (cavernInterior.IsSetup)
        {
            ValidateStates();
        }
        else
        {
            StartCoroutine(WaitForFinishSetup());
        }
    }

    private void ValidateStates()
    {
        bool dayStateBool = false;
        foreach (var dayState in dayStates)
        {
            dayStateBool = dayStateBool || GameManager.Instance.dayManager.IsCurrentDayState(dayState);
        }

        bool cavernType = false;
        foreach (var type in types)
        {
            cavernType = cavernType || cavernInterior.IsOfCavernType(type);
        }

        gameObject.SetActive(cavernType && dayStateBool);
    }

    private IEnumerator WaitForFinishSetup()
    {
        while (!cavernInterior.IsSetup)
        {
            yield return null;
        }
        ValidateStates();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
