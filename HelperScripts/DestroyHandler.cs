﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyHandler : MonoBehaviour
{
    //Ensure there's only one object of this type in the scene, otherwise destroy this one
    private void Awake()
    {
        //If there is more than one object in the scene with this tag, destroy this one to prevent duplicates
        if (GameObject.FindGameObjectsWithTag(this.transform.tag).Length > 1)
        {

            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }
}
