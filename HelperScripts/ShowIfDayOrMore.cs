﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowIfDayOrMore : MonoBehaviour
{

    public GameObject objectToEnable;
    public int dayCount = 2;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        if (GameManager.Instance.dayManager.GetCurrentDayNumber() >= dayCount)
        {
            objectToEnable.SetActive(true);
        }
        else
        {
            objectToEnable.SetActive(false);
        }
    }

    
}
