﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideIfEncounterState : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private Button buttonToDisable;
    
    private void OnEnable()
    {
        if (GameManager.Instance.globalCanvasUI)
        {
            if (buttonToDisable)
            {
                buttonToDisable.interactable = !GameManager.Instance.globalCanvasUI.IsInEncounterState();
            }
            else
            {
                gameObject.SetActive(!GameManager.Instance.globalCanvasUI.IsInEncounterState());
            }
        }
    }


    private void Update()
    {
        if (buttonToDisable)
        {
            buttonToDisable.interactable = !GameManager.Instance.globalCanvasUI.IsInEncounterState();
        }
        else
        {
            gameObject.SetActive(!GameManager.Instance.globalCanvasUI.IsInEncounterState());
        }
    }
}
