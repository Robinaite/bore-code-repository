﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowRotating : MonoBehaviour
{
    public float rotationSpeed = 0;
    
    private static float rotationValue = 0;

    private void Start()
    {
        transform.localRotation = Quaternion.Euler(0,rotationValue,0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,rotationSpeed * Time.deltaTime,0);
    }

    private void OnDestroy()
    {
        rotationValue = transform.localEulerAngles.y;
    }
}
